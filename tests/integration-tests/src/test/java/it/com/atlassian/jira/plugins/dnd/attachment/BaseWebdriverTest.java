package it.com.atlassian.jira.plugins.dnd.attachment;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.jira.testkit.client.IssueTypeControl.IssueType;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.TestedProductFactory;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;

import static java.lang.Long.parseLong;

public abstract class BaseWebdriverTest extends BaseJiraWebTest
{
    protected static final JiraTestedProduct JIRA = TestedProductFactory.create(JiraTestedProduct.class);
    protected static final PageBinder pageBinder = JIRA.getPageBinder();
    protected static final Backdoor backdoor = JIRA.backdoor();
    protected static final String USERNAME = "admin";
    protected static final String PROJECT_NAME = "Test project";
    protected static final String PROJECT_KEY = "TP";
    protected static final String ISSUE_SUMMARY = "DnD FTW";
    protected static final String TEST_FILE_NAME = "testfile.png";
    protected static final String BROKEN_ATTACHMENT_STORE = "/dev/null";
    protected static final String DEFAULT_PRIORITY = "1"; // expected to be "Blocker"

    /** Id of project created for testing. */
    protected static volatile long projectId;

    /** Issue key of issue created for testing. */
    protected static volatile String issueKey;

    protected static volatile IssueType primaryIssueType;
    protected static volatile IssueType secondaryIssueType;
    protected volatile static IssueType subTaskIssueType;
    protected static volatile long schemeId;
    private String originalAttachmentPath;

    @BeforeClass
    public static void setUpClass()
    {
        projectId = backdoor.project().addProject(PROJECT_NAME, PROJECT_KEY, jira.getAdminCredentials().getUsername());
        primaryIssueType = backdoor.issueType().createIssueType("Temporary primary issue type");
        secondaryIssueType = backdoor.issueType().createIssueType("Temporary secondary issue type");

        subTaskIssueType = new IssueTypeControl.IssueType();
        subTaskIssueType.setName("Temporary subtask issue type");
        subTaskIssueType.setIconUrl("/images/icons/genericissue.gif");
        subTaskIssueType.setSubtask(true);
        subTaskIssueType = backdoor.issueType().createIssueType(subTaskIssueType);

        issueKey = backdoor.issues().createIssue(PROJECT_KEY, ISSUE_SUMMARY, null, DEFAULT_PRIORITY, primaryIssueType.getId()).key();

        boolean allowAttachments = backdoor.applicationProperties().getOption(APKeys.JIRA_OPTION_ALLOWATTACHMENTS);
        if (!allowAttachments) {
            logger.warn("Attachments are disabled, should be enabled by default, re-enabling.");
            backdoor.applicationProperties().setOption(APKeys.JIRA_OPTION_ALLOWATTACHMENTS, true);
        }
    }

    @AfterClass
    public static void tearDownClass()
    {
        backdoor.issues().deleteIssue(issueKey, false);
        backdoor.issueType().deleteIssueType(parseLong(primaryIssueType.getId()));
        backdoor.issueType().deleteIssueType(parseLong(secondaryIssueType.getId()));
        backdoor.issueType().deleteIssueType(parseLong(subTaskIssueType.getId()));
        backdoor.project().deleteProject(PROJECT_KEY);
    }

    @Before
    public void setUp()
    {
        JIRA.quickLoginAsAdmin();
        JIRA.gotoHomePage();
        originalAttachmentPath = backdoor.attachments().getAttachmentPath();

        schemeId = backdoor.permissionSchemes().copyDefaultScheme("DND-TP-TEST-SCHEME");
        backdoor.project().setPermissionScheme(projectId, schemeId);
    }

    @After
    public void tearDown()
    {
        backdoor.project().setPermissionScheme(projectId, 0l);
        backdoor.permissionSchemes().deleteScheme(schemeId);

        fixAttachmentStore();
    }

    void fixAttachmentStore()
    {
        backdoor.attachments().setAttachmentPath(originalAttachmentPath);
    }

    void breakAttachmentStore()
    {
        backdoor.attachments().setAttachmentPath(BROKEN_ATTACHMENT_STORE);
    }

}
