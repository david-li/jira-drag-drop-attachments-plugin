package it.com.atlassian.plugins.jira.screenshot.webdriver;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.backdoor.Backdoor;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.testkit.client.IssueTypeControl;
import com.atlassian.jira.testkit.client.restclient.Attachment;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.webdriver.testing.rule.JavaScriptErrorsRule;
import org.junit.*;

import javax.inject.Inject;
import java.util.List;

public abstract class BasePasteWebTest extends BaseJiraWebTest
{
    protected final static JiraTestedProduct JIRA = TestedProductFactory.create(JiraTestedProduct.class);

    protected final static PageBinder pageBinder = JIRA.getPageBinder();

    protected final static Backdoor backdoor = JIRA.backdoor();

    protected final static String USERNAME = "admin", PROJECT_NAME = "Test project", PROJECT_KEY = "TP", ISSUE_SUMMARY = "DnD FTW";
    protected static final String FILE_NAME = "image.png";

    protected static volatile long projectId;

    protected static volatile String issueKey;
    protected static volatile IssueTypeControl.IssueType primaryIssueType;

    @Inject
    protected TraceContext traceContext;

    @Inject
    protected PageElementFinder finder;

    @Rule
    public JavaScriptErrorsRule javaScriptErrorsRule = new JavaScriptErrorsRule();

    @BeforeClass
    public static void setUpClass()
    {
        projectId = backdoor.project().addProject(PROJECT_NAME, PROJECT_KEY, USERNAME);
        primaryIssueType = backdoor.issueType().createIssueType("Temporary primary issue type");
    }

    @AfterClass
    public static void tearDownClass()
    {
        backdoor.issueType().deleteIssueType(Long.parseLong(primaryIssueType.getId()));
        backdoor.project().deleteProject(PROJECT_KEY);
        backdoor.fieldConfiguration().setFieldRenderer(FunctTestConstants.DEFAULT_FIELD_CONFIGURATION, FunctTestConstants.FIELD_COMMENT, FunctTestConstants.WIKI_STYLE_RENDERER);
        backdoor.fieldConfiguration().setFieldRenderer(FunctTestConstants.DEFAULT_FIELD_CONFIGURATION, "description", FunctTestConstants.WIKI_STYLE_RENDERER);
    }

    @Before
    public void setUp()
    {
        issueKey = backdoor.issues().createIssue(projectId, ISSUE_SUMMARY).key();

        JIRA.quickLoginAsAdmin();
        JIRA.gotoHomePage();
    }

    @After
    public void tearDown()
    {
        backdoor.issues().deleteIssue(issueKey, true);
    }

    protected int getAttachmentCount()
    {
        return ((List<Attachment>) backdoor.issues().getIssue(issueKey).fields.get("attachment")).size();
    }
}
