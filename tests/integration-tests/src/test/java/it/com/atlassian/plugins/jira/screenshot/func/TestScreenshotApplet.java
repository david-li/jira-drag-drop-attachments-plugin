package it.com.atlassian.plugins.jira.screenshot.func;

import com.atlassian.jira.functest.framework.BaseJiraFuncTest;
import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.google.common.collect.Lists;
import com.meterware.httpunit.HeadMethodWebRequest;
import com.meterware.httpunit.WebClient;
import com.meterware.httpunit.WebResponse;
import org.junit.Test;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.List;

import static com.atlassian.jira.functest.framework.suite.Category.FUNC_TEST;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;

@WebTest(FUNC_TEST)
@LoginAs(user = FunctTestConstants.ADMIN_USERNAME)
public class TestScreenshotApplet extends BaseJiraFuncTest
{
    private static final String RESOURCE_URL_PREFIX = "/download/resources/com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources/";
    private static final List<String> JAR_URLS = Lists.newArrayList(
            RESOURCE_URL_PREFIX + "clipboard.jar",
            RESOURCE_URL_PREFIX + "clipboard-legacy.jar"
    );

    // regression test for JRADEV-19430
    @Test
    public void testScreenshotAppletsShouldBeIncludedInPluginJar() throws Exception
    {
        for (String url : JAR_URLS)
        {
            WebResponse response = head(environmentData.getBaseUrl() + url);
            assertThat("HEAD " + url + " should return status code=200", response.getResponseCode(), equalTo(200));
        }
    }

    /**
     * Perform a HTTP HEAD request to a URL.
     * @param url The URL to make a HEAD request to.
     * @return The HTTP response.
     * @throws java.io.IOException
     * @throws org.xml.sax.SAXException
     */
    private WebResponse head(String url) throws IOException, SAXException
    {
        WebClient webClient = tester.getDialog().getWebClient();
        webClient.setExceptionsThrownOnErrorStatus(false);
        try
        {
            return webClient.sendRequest(new HeadMethodWebRequest(url));
        }
        finally
        {
            webClient.setExceptionsThrownOnErrorStatus(true);
        }
    }
}
