package it.com.atlassian.jira.plugins.dnd.attachment;

import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.elements.AuiFlag;
import com.atlassian.jira.pageobjects.elements.GlobalFlags;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.AttachmentsDropZone;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.DropZone;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.UploadProgressBar;
import com.atlassian.test.categories.OnDemandAcceptanceTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import java.util.List;

import javax.inject.Inject;

import static com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

@WebTest ({ WEBDRIVER_TEST })
@Category ({ OnDemandAcceptanceTest.class })

public class TestErrorMessages extends BaseWebdriverTest
{
    @Inject
    protected TraceContext traceContext;

    @Test
    public void testAttachmentsModuleAttachFileError()
    {
        backdoor.flags().enableFlags();

        JIRA.goToViewIssue(issueKey);

        Tracer tracer = traceContext.checkpoint();

        DropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME);

        traceContext.waitFor(tracer, "jira.issue.dnd.attached");

        GlobalFlags flags = pageBinder.bind(GlobalFlags.class);
        AuiFlag flag = flags.getFlagWithText(TEST_FILE_NAME);
        assertEquals("Message flag is success", AuiFlag.Type.SUCCESS, flag.getType());
        flag.dismiss();

        breakAttachmentStore();

        tracer = traceContext.checkpoint();
        dropZone.dropImage(TEST_FILE_NAME);
        traceContext.waitFor(tracer, "jira.issue.dnd.commit.fail");

        assertEquals("Message flag is error", AuiFlag.Type.ERROR, flag.getType());

        final List<UploadProgressBar> progressBars = dropZone.getProgressBars();

        assertEquals("There is only one progress bar visible", 1, progressBars.size());
        final UploadProgressBar progressBar = progressBars.get(0);
        assertEquals("Progress bar is in error state", UploadProgressBar.Status.ERROR, progressBar.getStatus());

        tracer = traceContext.checkpoint();
        flag.dismiss();
        traceContext.waitFor(tracer, "jira.issue.dnd.progressbar.removed");

        assertFalse("Progress bar is not present", progressBar.isPresent());
    }
}
