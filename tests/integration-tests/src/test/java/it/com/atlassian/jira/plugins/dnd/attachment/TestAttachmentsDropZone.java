package it.com.atlassian.jira.plugins.dnd.attachment;

import com.atlassian.jira.functest.framework.FunctTestConstants;
import com.atlassian.jira.functest.framework.navigation.issue.AttachmentsBlock;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.elements.AuiFlag;
import com.atlassian.jira.pageobjects.elements.GlobalFlags;
import com.atlassian.jira.pageobjects.pages.viewissue.AddCommentSection;
import com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage;
import com.atlassian.jira.pageobjects.pages.viewissue.attachment.AttachmentSection;
import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.permission.JiraPermissionHolderType;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.AttachmentsDropZone;
import com.atlassian.jira.plugins.dnd.attachment.pageobjects.AttachmentsSection;
import com.atlassian.test.categories.OnDemandAcceptanceTest;
import org.junit.After;
import org.junit.Test;
import org.junit.experimental.categories.Category;

import javax.inject.Inject;
import java.util.List;

import static com.atlassian.jira.functest.framework.suite.Category.WEBDRIVER_TEST;
import static org.junit.Assert.*;

@WebTest ({ WEBDRIVER_TEST })
@Category ({ OnDemandAcceptanceTest.class })
public class TestAttachmentsDropZone extends BaseWebdriverTest
{
    @Inject
    private TraceContext traceContext;

    @Override
    public void setUp()
    {
        super.setUp();
        issueKey = backdoor.issues().createIssue(projectId, "Empty attachments").key();
    }

    @After
    public void tearDown()
    {
        super.tearDown();
        backdoor.issues().deleteIssue(issueKey, true);
    }

    @Test
    public void testEmptyAttachments()
    {
        ViewIssuePage issuePage = JIRA.goToViewIssue(issueKey);
        assertTrue("drop zone is visible", pageBinder.bind(AttachmentsDropZone.class).isVisible());

        AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        dropZone.attachImage(TEST_FILE_NAME);

        List<String> fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();
        assertEquals("There is one attachment", 1, fileNames.size());
        assertEquals("File name equals '" + TEST_FILE_NAME + "'", fileNames.get(0), TEST_FILE_NAME);
    }

    @Test
    public void testViewModeToggle()
    {
        JIRA.goToViewIssue(issueKey);
        List<String> fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();

        assertTrue("drop zone is visible", pageBinder.bind(AttachmentsDropZone.class).isVisible());
        assertEquals("There are no attachments", 0, fileNames.size());

        pageBinder.bind(AttachmentsDropZone.class).attachImage(TEST_FILE_NAME);

        pageBinder.bind(AttachmentSection.class).openOptions().setViewMode(AttachmentsBlock.ViewMode.LIST);
        pageBinder.bind(AttachmentsDropZone.class).attachImage(TEST_FILE_NAME);

        pageBinder.bind(AttachmentSection.class).openOptions().setViewMode(AttachmentsBlock.ViewMode.GALLERY);
        pageBinder.bind(AttachmentsDropZone.class).attachImage(TEST_FILE_NAME);

        pageBinder.bind(AttachmentSection.class).openOptions().setViewMode(AttachmentsBlock.ViewMode.LIST);
        fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();
        assertEquals("There are 3 attachments", 3, fileNames.size());
    }

    @Test
    public void testSortByToggle()
    {
        final String firstFileName = "aaa_file.png";
        final String secondFileName = "bbb_file.png";

        JIRA.goToViewIssue(issueKey);
        List<String> fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();

        assertTrue("drop zone is visible", pageBinder.bind(AttachmentsDropZone.class).isVisible());
        assertEquals("There are no attachments", 0, fileNames.size());

        Tracer tracer = traceContext.checkpoint();
        pageBinder.bind(AttachmentsDropZone.class).dropImage(firstFileName, null);
        pageBinder.bind(AttachmentSection.class).openOptions().setSortBy(AttachmentsBlock.Sort.Key.DATE);
        traceContext.waitFor(tracer, "jira.issue.dnd.attached");

        tracer = traceContext.checkpoint();
        pageBinder.bind(AttachmentsDropZone.class).dropImage(secondFileName, null);
        pageBinder.bind(AttachmentSection.class).openOptions().setSortOrder(AttachmentsBlock.Sort.Direction.DESCENDING);
        traceContext.waitFor(tracer, "jira.issue.dnd.attached");

        fileNames = pageBinder.bind(AttachmentsSection.class).getFileNames();
        assertEquals("There are 2 files", 2, fileNames.size());

        assertEquals("Last uploaded file is first on the list", secondFileName, fileNames.get(0));
    }

    @Test
    public void shouldCancelAttachmentAfterFailedUpload()
    {
        Tracer tracer;
        ViewIssuePage issuePage = JIRA.goToViewIssue(issueKey);
        assertTrue("drop zone is visible", pageBinder.bind(AttachmentsDropZone.class).isVisible());

        breakAttachmentStore();

        tracer = traceContext.checkpoint();
        AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        dropZone.dropImage(TEST_FILE_NAME);
        traceContext.waitFor(tracer, "jira.issue.dnd.commit.fail");

        dropZone.getProgressBar(TEST_FILE_NAME).cancel();

        assertEquals("Drop zone is empty", 0, dropZone.getFileNames().size());

        tracer = traceContext.checkpoint();
        dropZone.dropImage(TEST_FILE_NAME);
        traceContext.waitFor(tracer, "jira.issue.dnd.commit.fail");

        GlobalFlags flags = pageBinder.bind(GlobalFlags.class);
        AuiFlag flag = flags.getFlagWithText(TEST_FILE_NAME);

        // restore correct attachment settings, so it does not break attachment panel
        fixAttachmentStore();
        pageBinder.bind(AttachmentSection.class).openOptions().setSortOrder(AttachmentsBlock.Sort.Direction.DESCENDING);

        dropZone = pageBinder.bind(AttachmentsDropZone.class);
        dropZone.getProgressBar(TEST_FILE_NAME).cancel();

        assertEquals("Drop zone is empty", 0, dropZone.getFileNames().size());
    }

    @Test
    public void whenUnifiedAttachmentsEnabledFullDropzonePresent()
    {
        JIRA.goToViewIssue(issueKey);
        AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        assertTrue(dropZone.getFullPageDropZone().isPresent());
    }

    @Test
    public void whenUnifiedAttachmentsEnabledDropZoneAppearsWhenDragoverIssue()
    {
        JIRA.goToViewIssue(issueKey);
        AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        Tracer tracer = traceContext.checkpoint();
        dropZone.dragoverIssue();
        traceContext.waitFor(tracer, "jira.issue.dnd.dragover");
    }

    @Test
    public void whenUnifiedAttachmentsEnabledWhenEditingFieldDropAddsMarkup()
    {
        ViewIssuePage issuePage = JIRA.goToViewIssue(issueKey);
        AddCommentSection comment = issuePage.comment();
        AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        dropZone.dropOntoFullDropzone(TEST_FILE_NAME);
        // The textfield isn't populated until it has focus, so manually give it focus
        comment.typeComment("");
        assertEquals("Comment should be filled with wiki markup for image", "!testfile.png|thumbnail!", comment.getComment().trim());
    }

    @Test
    public void whenUnifiedAttachmentsEnabledCannotDropWithoutPermission()
    {
        backdoor.permissionSchemes().removeProjectRolePermission(schemeId, ProjectPermissions.CREATE_ATTACHMENTS, FunctTestConstants.JIRA_USERS_ROLE_ID);
        backdoor.permissionSchemes().removePermission(schemeId, ProjectPermissions.CREATE_ATTACHMENTS, JiraPermissionHolderType.APPLICATION_ROLE.getKey()).parameter("").getRequest();
        JIRA.goToViewIssue(issueKey);
        AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        assertFalse(dropZone.getFullPageDropZone().isPresent());
    }

    @Test
    public void whenUnifiedAttachmentsEnabledCannotFullPageDropWhenDialogOpen()
    {
        ViewIssuePage issuePage = JIRA.goToViewIssue(issueKey);
        issuePage.editIssue();
        AttachmentsDropZone dropZone = pageBinder.bind(AttachmentsDropZone.class);
        Tracer tracer = traceContext.checkpoint();
        dropZone.dragoverIssue();
        traceContext.waitFor(tracer, "jira.issue.dnd.dropnotallowed");
    }
}
