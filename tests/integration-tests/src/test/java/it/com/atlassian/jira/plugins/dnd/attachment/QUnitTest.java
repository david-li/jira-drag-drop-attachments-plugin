package it.com.atlassian.jira.plugins.dnd.attachment;

import com.atlassian.jira.functest.framework.suite.Category;
import com.atlassian.jira.functest.framework.suite.WebTest;
import com.atlassian.jira.pageobjects.BaseJiraWebTest;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.qunit.test.runner.QUnitPageObjectsHelper;
import com.atlassian.qunit.test.runner.QUnitTestMeta;
import com.atlassian.qunit.test.runner.QUnitTestResult;
import org.apache.commons.lang.StringUtils;
import org.junit.After;
import org.junit.Test;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.io.File;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

@WebTest({ Category.WEBDRIVER_TEST, Category.QUNIT })
public class QUnitTest extends BaseJiraWebTest
{
    @Inject
    private PageElementFinder elementFinder;

    private final File outdir;

    public QUnitTest() {
        String location = System.getProperty("jira.qunit.testoutput.location");

        if (StringUtils.isEmpty(location))
        {
            System.err.println("Writing result XML to tmp, jira.qunit.testoutput.location not defined");
            location = System.getProperty("java.io.tmpdir");
        }

        outdir = new File(location);
    }

    @After
    public void tearDown()
    {
        jira.gotoHomePage();
        final PageElement resetLink = elementFinder.find(By.cssSelector("#reset-lnk"));
        if(resetLink.isPresent())
        {
            resetLink.click();
        }
        else
        {
            System.err.println("Locale was not reset, couldn't find link");
        }
    }

    @Test
    public void runJustOurTest() throws Exception {
        QUnitPageObjectsHelper helper = new QUnitPageObjectsHelper(outdir, jira.getPageBinder());
        List<QUnitTestMeta> tests = helper.getTests(QUnitPageObjectsHelper.testFilePathContains("jira-dnd-attachment-plugin"));

        assertTrue("Found some tests", tests.size() > 0);

        for (QUnitTestMeta test : tests) {
            QUnitTestResult testResult = helper.runTest(test);
            for(QUnitTestResult.TestResult result : testResult.getResults())
                for(QUnitTestResult.LogMessage log : result.log)
                    if(!log.result)
                        fail(log.message);

            assertTrue(test.getSuiteName() + " passed", testResult.getFailures() == 0);
        }
    }
}
