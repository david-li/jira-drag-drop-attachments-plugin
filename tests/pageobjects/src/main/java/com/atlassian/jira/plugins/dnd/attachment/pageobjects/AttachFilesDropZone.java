package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.inject.Inject;
import org.openqa.selenium.By;

public class AttachFilesDropZone extends FormDropZone {

    @ElementBy(cssSelector = ".issue-drop-zone[duiType='dndattachment/dropzones/AttachFilesDropZone'].-dui-type-parsed")
    private PageElement dropZoneElement;

    @ElementBy(cssSelector = "#attach-file-dialog")
    private PageElement attachFileElement;

    public PageElement getDropZoneElement() {
        return dropZoneElement;
    }

    public By getProgressBarLocator() {
        return By.cssSelector(".issue-drop-zone[duiType='dndattachment/dropzones/AttachFilesDropZone']~.upload-progress-bar");
    }

    public PageElement getContextElement() {
        return attachFileElement;
    }
}
