package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugins.dnd.attachment.util.TestUtil;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import java.util.List;

public abstract class DropZone
{
    public static String DRAGOVER_CLASS = "issue-drop-zone-document__dragover";

    @javax.inject.Inject
    protected TraceContext traceContext;

    public abstract PageElement getDropZoneElement();

    public void dropImage(String fileName)
    {
        Tracer tracer = traceContext.checkpoint();
        dropImage(fileName, tracer);
    }


    public void dropImage(String fileName, Tracer tracer)
    {
        getDropZoneElement().javascript().execute(TestUtil.readResource("js/test-util.js"), fileName);
        if(tracer != null)
        {
            traceContext.waitFor(tracer, "jira.issue.dnd.uploaded");
        }
    }

    public void dropFile(String text, String fileName)
    {
        dropFile(text, fileName, traceContext.checkpoint());
    }

    public void dropFile(String text, String fileName, Tracer tracer)
    {
        getDropZoneElement().javascript().execute(TestUtil.readResource("js/simulate-drop-file.js"), text, fileName);
        traceContext.waitFor(tracer, "jira.issue.dnd.uploaded");
    }

    public boolean isVisible()
    {
        return getDropZoneElement().timed().isVisible().byDefaultTimeout();
    }

    public boolean isAvailable()
    {
        return getDropZoneElement().timed().isPresent().byDefaultTimeout();
    }

    public abstract By getProgressBarLocator();

    public abstract PageElement getContextElement();

    private List<PageElement> getFiles()
    {
        return getContextElement().findAll(getProgressBarLocator());
    }

    public List<String> getFileNames()
    {
        return Lists.newArrayList(Lists.transform(getFiles(), new Function<PageElement, String>()
        {
            public String apply(PageElement element)
            {
                return element.find(By.className("upload-progress-bar__file-name")).getText();
            }
        }));
    }

    public UploadProgressBar getProgressBar(final String fileName)
    {
        PageElement element = Iterables.getOnlyElement(Iterables.filter(getFiles(), new Predicate<PageElement>()
        {
            @Override
            public boolean apply(final PageElement element)
            {
                return element.find(By.className("upload-progress-bar__file-name")).getText().equals(fileName);
            }
        }), null);

        if (element == null)
        {
            throw new NoSuchElementException(String.format("Unable to find progress bar for file name: %s", fileName));
        }
        else
        {
            return new UploadProgressBar(element);
        }
    }

    public List<UploadProgressBar> getProgressBars()
    {
        return Lists.transform(getFiles(), new Function<PageElement, UploadProgressBar>()
        {
            @Override
            public UploadProgressBar apply(final PageElement element)
            {
                return new UploadProgressBar(element);
            }
        });
    }

}
