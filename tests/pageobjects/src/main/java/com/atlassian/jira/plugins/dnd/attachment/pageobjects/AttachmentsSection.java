package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.PageElementFinder;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.google.common.base.Function;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;

import java.util.List;
import javax.inject.Inject;

public class AttachmentsSection
{

    @Inject
    protected PageElementFinder elementFinder;

    @ElementBy (id = "attachmentmodule")
    private PageElement attachmentPanel;

    public boolean isVisible()
    {
        return attachmentPanel.timed().isVisible().byDefaultTimeout();
    }

    public List<PageElement> getFiles()
    {
        List<PageElement> elements;
        try
        {
            elements = attachmentPanel.withTimeout(TimeoutType.COMPONENT_LOAD).findAll(By.cssSelector(".attachment-title a"));
        }
        catch (NoSuchElementException exception)
        {
            return ImmutableList.<PageElement>of();
        }
        elements.addAll(attachmentPanel.withTimeout(TimeoutType.COMPONENT_LOAD).findAll(By.cssSelector("a.attachment-title")));

        return elements;
    }

    public List<String> getFileNames() {
        return Lists.newArrayList(Lists.transform(getFiles(), new Function<PageElement, String>()
        {
            @Override
            public String apply(PageElement element)
            {
                return element.getText().trim();
            }
        }));
    }

    public int getFileCount()
    {
        return getFiles().size();
    }

}
