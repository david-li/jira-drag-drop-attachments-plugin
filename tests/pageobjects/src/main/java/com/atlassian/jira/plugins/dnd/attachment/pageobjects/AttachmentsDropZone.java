package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugins.dnd.attachment.util.TestUtil;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.google.common.base.Function;
import com.google.common.collect.Iterables;
import com.google.inject.Inject;
import org.openqa.selenium.By;

public class AttachmentsDropZone extends DropZone {

    @ElementBy(cssSelector = ".issue-drop-zone[duiType='dndattachment/dropzones/AttachmentsDropZone'].-dui-type-parsed")
    private PageElement dropZoneElement;

    @ElementBy(cssSelector = "#attachmentmodule")
    private PageElement dndPanel;

    @ElementBy(cssSelector = ".attachments-drop-zone__dragover-border")
    private PageElement fullPageDropZone;

    @ElementBy(cssSelector = ".issue-container")
    private PageElement issueContainer;

    public PageElement getDropZoneElement() {
        return dropZoneElement;
    }

    public By getProgressBarLocator() {
        return By.cssSelector(".attachments-upload-progress-bar");
    }

    public PageElement getContextElement() {
        return dndPanel;
    }

    public PageElement getFullPageDropZone()
    {
        return fullPageDropZone;
    }

    public void attachImage(String fileName) {
        Tracer tracer = traceContext.checkpoint();

        dropImage(fileName, tracer);

        traceContext.waitFor(tracer, "jira.issue.dnd.attached");

    }

    public void dragoverIssue()
    {
        issueContainer.javascript().execute(TestUtil.readResource("js/simulate-dragover.js"));
    }

    public void dropOntoFullDropzone(String fileName)
    {
        Tracer tracer = traceContext.checkpoint();
        fullPageDropZone.javascript().execute(TestUtil.readResource("js/simulate-full-page-drop.js"), fileName);
        traceContext.waitFor(tracer, "jira.issue.dnd.attached");
    }

}