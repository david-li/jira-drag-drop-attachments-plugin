package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.jira.pageobjects.pages.AbstractJiraPage;
import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class CreateIssueDetailsPage extends AbstractJiraPage {
    @ElementBy (id = "issue-create-submit")
    PageElement submit;

    @ElementBy (id = "summary")
    PageElement summary;

    @Override
    public TimedCondition isAt()
    {
        return submit.timed().isVisible();
    }

    @Override
    public String getUrl()
    {
        throw new UnsupportedOperationException("Not implemented");
    }

    public void setSummary(String summary) {
        this.summary.type(summary);
    }

    public void submit() {
        this.submit.click();
    }
}
