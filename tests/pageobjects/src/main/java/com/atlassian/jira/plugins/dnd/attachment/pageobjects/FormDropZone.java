package com.atlassian.jira.plugins.dnd.attachment.pageobjects;

import com.atlassian.jira.pageobjects.util.Tracer;

public abstract class FormDropZone extends DropZone
{

    @Override
    public void dropImage(final String fileName)
    {
        Tracer tracer = traceContext.checkpoint();
        dropImage(fileName, tracer);
        traceContext.waitFor(tracer, "jira.issue.dnd.isclear");
        traceContext.waitFor(tracer, "jira.issue.dnd.uploadfinished");
    }
}
