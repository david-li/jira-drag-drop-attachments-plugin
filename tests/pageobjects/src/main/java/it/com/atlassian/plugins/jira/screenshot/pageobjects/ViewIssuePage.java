package it.com.atlassian.plugins.jira.screenshot.pageobjects;

import com.atlassian.jira.pageobjects.util.TraceContext;
import com.atlassian.jira.pageobjects.util.Tracer;
import com.atlassian.jira.plugin.issuenav.pageobjects.IssueDetailComponent;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.Field;
import com.atlassian.jira.plugin.issuenav.pageobjects.fields.InlineDescriptionField;
import com.atlassian.pageobjects.PageBinder;
import com.atlassian.pageobjects.ProductInstance;
import com.atlassian.pageobjects.elements.PageElement;
import org.openqa.selenium.By;

import javax.inject.Inject;
import java.util.regex.Pattern;

public class ViewIssuePage extends com.atlassian.jira.pageobjects.pages.viewissue.ViewIssuePage
{

    @Inject
    private PageBinder pageBinder;

    @Inject
    private ProductInstance jiraProduct;

    @Inject
    protected TraceContext traceContext;

    public ViewIssuePage(String issueKey)
    {
        super(issueKey);
    }

    public void pasteImage(String fileName)
    {
        pasteImage(body, fileName, true);
    }

    public void pasteImage(PageElement element, String fileName)
    {
        pasteImage(element, fileName, true);
    }

    public void pasteMultiContentRtfAndImage(PageElement element, String fileName)
    {
        Tracer tracer = traceContext.checkpoint();
        PasteEventEmulator.emulatePasteMultiContentRtfAndImage(element, fileName);
        traceContext.waitFor(tracer, "jira/attach-images-plugin/pasteIgnoredNotImage");
    }

    public void pasteImage(String fileName, boolean trace)
    {
        pasteImage(body, fileName, trace);
    }

    public void pasteImage(PageElement element, String fileName, boolean trace)
    {
        Tracer tracer = traceContext.checkpoint();
        PasteEventEmulator.emulatePaste(element, fileName);
        if (trace)
        {
            traceContext.waitFor(tracer, "AJS.$.ajaxComplete", Pattern.compile(jiraProduct.getContextPath() + "/rest/internal/1.0/AttachTemporaryFile(.+)"));
        }
    }

    public void pasteImageWithoutWait(String fileName)
    {
        pasteImageWithoutWait(body, fileName, true);
    }

    public void pasteImageWithoutWait(PageElement element, String fileName, boolean trace)
    {
        Tracer tracer = traceContext.checkpoint();
        PasteEventEmulator.emulatePaste(element, fileName);
    }

    public void pasteImageWithoutWait(PageElement element, String fileName)
    {
        pasteImageWithoutWait(element, fileName, true);
    }

    public PageElement getCommentField()
    {
        return body.find(By.id("comment"));
    }

    public PageElement goToDescriptionEdit()
    {
        IssueDetailComponent issueDetails = pageBinder.bind(IssueDetailComponent.class, new Object[] { this.getIssueKey() });
        final InlineDescriptionField description = issueDetails.description();

        Field descriptionEdit = description.switchToEdit();
        return body.find(By.id(descriptionEdit.getId()));
    }
}
