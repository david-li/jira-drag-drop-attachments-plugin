// Drop event emulator

var evt = jQuery.Event( "drop" );
evt.dataTransfer = {files: [ document.createElement('canvas').mozGetAsFile(arguments[1]) ]};
AJS.$(arguments[0]).trigger(evt);