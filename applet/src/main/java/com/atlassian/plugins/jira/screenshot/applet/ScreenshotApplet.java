/*
 * Copyright (c) 2002-2006
 * All rights reserved.
 */

package com.atlassian.plugins.jira.screenshot.applet;

import netscape.javascript.JSObject;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.xml.bind.DatatypeConverter;
import java.applet.Applet;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.image.BufferedImage;
import java.awt.image.ImageProducer;
import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.security.AccessControlException;
import java.security.AccessController;
import java.security.PrivilegedAction;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Applet for reading data from clipboard and proxying ajax requests to server
 *
 * @author gtanczyk
 */
public class ScreenshotApplet extends Applet {

    private static final String JSESSIONID = "JSESSIONID";
    private static final String DATA_FORMAT = "JPEG";
    private static final String DATA_MIME_TYPE = "image/jpeg";

    private BufferedImage bufferedImage;

    private String sessionId = null;

    private ExecutorService executorService = Executors.newSingleThreadExecutor();

    public boolean isSecurityOk() {
        return AccessController.<Boolean>doPrivileged(new PrivilegedAction() {
            public Boolean run() {
                try {
                    Toolkit.getDefaultToolkit().getSystemClipboard();
                    return true;
                } catch (final AccessControlException e) {
                    e.printStackTrace();
                    return false;
                }
            }
        });
    }

    public void reload() {
        Image image = null;
        try {
            final Transferable t =
                    AccessController.<Transferable>doPrivileged(new PrivilegedAction() {
                        public Transferable run() {
                            return Toolkit.getDefaultToolkit().getSystemClipboard().getContents(null);
                        }
                    });


            if ((t != null) && t.isDataFlavorSupported(new DataFlavor("image/x-java-image; class=java.awt.Image", "Image"))) {
                image = (Image) t.getTransferData(new DataFlavor("image/x-java-image; class=java.awt.Image", "Image"));
            } else if (isMacOSX()) {
                image = getMacImage(t);
            }

            bufferedImage = new BufferedImage(image.getWidth(this), image.getHeight(this), BufferedImage.TYPE_INT_RGB);
            final Graphics2D g = bufferedImage.createGraphics();
            g.drawImage(image, 0, 0, this);
            g.dispose();
        } catch (final UnsupportedFlavorException e) {
            System.out.println("Unsupported image flavor: " + e);
            e.printStackTrace();
        } catch (final IOException e) {
            System.out.println("IOException getting clipboard contents: " + e);
            e.printStackTrace();
        }
    }

    private boolean isMacOSX() {
        final String osname = System.getProperty("os.name");
        final boolean isWin = osname.startsWith("Windows");
        return !isWin && osname.startsWith("Mac");
    }

    public JSObject doMultipartRequest(final String url, final String encoding, final String userAgent, JSObject deferred) {
        return doRequest(new MultipartRequestTask(url, encoding, null, userAgent, deferred), url, encoding, userAgent, deferred, null);
    }

    public JSObject doAjaxRequest(final String url, final String encoding, final String userAgent, final JSObject deferred, final String... requestParams) {
        return doRequest(new AjaxRequestTask(url, encoding, requestParams, userAgent, deferred), url, encoding, userAgent, deferred, requestParams);
    }

    private <T extends RequestTask> JSObject doRequest(T requestTask, final String url, final String encoding, final String userAgent, final JSObject deferred, final String... requestParams) {
        executorService.execute(requestTask);

        return deferred;
    }

    private void writeJpegImage(final ByteArrayOutputStream os, final BufferedImage bufferedImage) throws IOException {
        final ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(os);
        final IIOImage ioImage = new IIOImage(bufferedImage, null, null);
        final ImageWriter writer = ImageIO.getImageWritersByFormatName("jpeg").next();
        final ImageWriteParam iwp = writer.getDefaultWriteParam();
        iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        iwp.setCompressionQuality(0.90f);
        writer.setOutput(imageOutputStream);
        writer.write(null, ioImage, iwp);
    }

    // ------------------------------------------------------------------------
    // Code below copied verbatim from http://rsb.info.nih.gov/ij/plugins/download/System_Clipboard.java
    // Part of the public domain 'ImageJ' library

    // Mac OS X's data transfer handling is horribly broken... we
    // need to use the "image/x-pict" MIME type and then Quicktime
    // for Java in order to obtain image data without problems.
    Image getMacImage(final Transferable t) {
        if (!isQTJavaInstalled()) {
            throw new RuntimeException("QuickTime for Java is not installed");
        }
        Image img = null;
        final DataFlavor[] d = t.getTransferDataFlavors();
        if ((d == null) || (d.length == 0)) {
            return null;
        }
        try {
            final Object is = t.getTransferData(d[0]);
            if ((is == null) || !(is instanceof InputStream)) {
                throw new RuntimeException("Clipboad does not appear to contain an image");
            }
            img = getImageFromPictStream((InputStream) is);
        } catch (final Exception e) {
        }
        return img;
    }

    // Converts a PICT to an AWT image using QuickTime for Java.
    // This code was contributed by Gord Peters.
    Image getImageFromPictStream(final InputStream is) {
        try {
            final ByteArrayOutputStream baos = new ByteArrayOutputStream();
            // We need to strip the header from the data because a PICT file
            // has a 512 byte header and then the data, but in our case we only
            // need the data. --GP
            final byte[] header = new byte[512];
            final byte[] buf = new byte[4096];
            int retval;
            baos.write(header, 0, 512);
            while ((retval = is.read(buf, 0, 4096)) > 0) {
                baos.write(buf, 0, retval);
            }
            baos.close();
            final int size = baos.size();
            //IJ.log("size: "+size); IJ.wait(2000);
            if (size <= 0) {
                return null;
            }
            final byte[] imgBytes = baos.toByteArray();
            // Again with the uglyness.  Here we need to use the Quicktime
            // for Java code in order to create an Image object from
            // the PICT data we received on the clipboard.  However, in
            // order to get this to compile on other platforms, we use
            // the Java reflection API.
            //
            // For reference, here is the equivalent code without
            // reflection:
            //
            //
            // if (QTSession.isInitialized() == false) {
            //     QTSession.open();
            // }
            // QTHandle handle= new QTHandle(imgBytes);
            // GraphicsImporter gi=
            //     new GraphicsImporter(QTUtils.toOSType("PICT"));
            // gi.setDataHandle(handle);
            // QDRect qdRect= gi.getNaturalBounds();
            // GraphicsImporterDrawer gid= new GraphicsImporterDrawer(gi);
            // QTImageProducer qip= new QTImageProducer(gid,
            //                          new Dimension(qdRect.getWidth(),
            //                                        qdRect.getHeight()));
            // return(Toolkit.getDefaultToolkit().createImage(qip));
            //
            // --GP
            //IJ.log("quicktime.QTSession");
            Class<?> c = Class.forName("quicktime.QTSession");
            Method m = c.getMethod("isInitialized");
            final Boolean b = (Boolean) m.invoke(null);
            if (!b.booleanValue()) {
                m = c.getMethod("open");
                m.invoke(null);
            }
            c = Class.forName("quicktime.util.QTHandle");
            Constructor<?> con = c.getConstructor(imgBytes.getClass());
            final Object handle = con.newInstance(new Object[]{imgBytes});
            final String s = "PICT";
            c = Class.forName("quicktime.util.QTUtils");
            m = c.getMethod("toOSType", s.getClass());
            final Integer type = (Integer) m.invoke(null, s);
            c = Class.forName("quicktime.std.image.GraphicsImporter");
            con = c.getConstructor(Integer.TYPE);
            final Object importer = con.newInstance(type);
            m = c.getMethod("setDataHandle", Class.forName("quicktime.util." + "QTHandleRef"));
            m.invoke(importer, handle);
            m = c.getMethod("getNaturalBounds");
            final Object rect = m.invoke(importer);
            c = Class.forName("quicktime.app.view.GraphicsImporterDrawer");
            con = c.getConstructor(importer.getClass());
            final Object iDrawer = con.newInstance(importer);
            m = rect.getClass().getMethod("getWidth");
            final Integer width = (Integer) m.invoke(rect);
            m = rect.getClass().getMethod("getHeight");
            final Integer height = (Integer) m.invoke(rect);
            final Dimension d = new Dimension(width.intValue(), height.intValue());
            c = Class.forName("quicktime.app.view.QTImageProducer");
            con = c.getConstructor(iDrawer.getClass(), d.getClass());
            final Object producer = con.newInstance(iDrawer, d);
            if (producer instanceof ImageProducer) {
                return (Toolkit.getDefaultToolkit().createImage((ImageProducer) producer));
            }
        } catch (final RuntimeException re) {
            System.out.println("Runtime Exception: " + re.getMessage());
            re.printStackTrace();
            throw re;
        } catch (final Exception e) {
            System.out.println("QuickTime for Java error: " + e.getMessage());
            e.printStackTrace();
            throw new RuntimeException("QuickTime for java error: " + e);
        }
        return null;
    }

    // Retuns true if QuickTime for Java is installed.
    // This code was contributed by Gord Peters.
    boolean isQTJavaInstalled() {
        boolean isInstalled;
        try {
            Class.forName("quicktime.QTSession");
            isInstalled = true;
        } catch (final Exception e) {
            isInstalled = false;
        }
        return isInstalled;
    }

    /**
     * Returns base64 encoded image, scaled to fit in targetWidth/targetHeight if necessary
     *
     * @return base64 encoded image
     */
    public String getClipboardData() {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            ImageIO.write(bufferedImage, DATA_FORMAT, out);
            return String.format("data:%s;base64,%s", DATA_MIME_TYPE, DatatypeConverter.printBase64Binary(out.toByteArray()));
        } catch (IOException e) {
            return null;
        }
    }

    /**
     * Returns base64 encoded image, scaled to fit in targetWidth/targetHeight if necessary
     *
     * @param maxWidth
     * @param maxHeight
     * @return base64 encoded image
     */
    public String getClipboardData(final int maxWidth, final int maxHeight) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        try {
            int width = bufferedImage.getWidth();
            int height = bufferedImage.getHeight();

            if ((maxWidth < width || maxHeight < height) && (height > 0)) {
                ImageIO.write(scaleImage(bufferedImage, width, height, maxWidth, maxHeight), DATA_FORMAT, out);
            } else {
                ImageIO.write(bufferedImage, DATA_FORMAT, out);
            }

            return String.format("data:%s;base64,%s", DATA_MIME_TYPE, DatatypeConverter.printBase64Binary(out.toByteArray()));
        } catch (IOException e) {
            return null;
        }
    }

    private static final BufferedImage scaleImage(final BufferedImage bufferedImage, final int width, final int height, final int maxWidth, final int maxHeight) {
        System.out.println("Scaling image: " + maxWidth + " " + maxHeight + "; " + width + " " + height);

        float ratio = (float) width / height;

        final int targetWidth;
        final int targetHeight;

        if (width > maxWidth) {
            targetWidth = maxWidth;
            targetHeight = (int) (maxWidth / ratio);
        } else {
            targetWidth = (int) (maxHeight * ratio);
            targetHeight = maxHeight;
        }

        BufferedImage scaledImage = new BufferedImage(targetWidth, targetHeight, bufferedImage.getType());
        Graphics2D g2d = scaledImage.createGraphics();

        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.drawImage(bufferedImage, 0, 0, targetWidth, targetHeight, 0, 0, width, height, null);
        g2d.dispose();

        return scaledImage;
    }

    /**
     * Request task for file uploads
     */
    private class MultipartRequestTask extends RequestTask {

        private byte[] data;

        public MultipartRequestTask(String url, String encoding, String[] requestParams, String userAgent, JSObject deferred) {
            super(url, encoding, requestParams, userAgent, deferred);
        }

        @Override
        protected HttpURLConnection prepareConnection(final String url) throws IOException
        {
            final ByteArrayOutputStream os = new ByteArrayOutputStream();
            writeJpegImage(os, bufferedImage);
            os.close();
            data = os.toByteArray();

            // it is easier to set uploaded file size here, rather than pass it back and forth from applet to js and v-ce versa
            if(requestParams != null)
            {
                for (int i = 0; i < requestParams.length; i++)
                {
                    if ("size".equals(requestParams[i]) && "-1".equals(requestParams[i + 1]))
                    {
                        requestParams[i + 1] = Integer.toString(os.size());
                    }
                }
            }

            return super.prepareConnection(url.replace("size=-1", String.format("size=%d", os.size())));
        }

        @Override
        public byte[] prepareConnectionData(URLConnection connection) throws IOException {
            connection.setRequestProperty("Content-type", "image/jpeg");

            return data;
        }
    }

    /**
     * Request task for submitting form data
     */
    private class AjaxRequestTask extends RequestTask {

        public AjaxRequestTask(String url, String encoding, String[] requestParams, String userAgent, JSObject deferred) {
            super(url, encoding, requestParams, userAgent, deferred);
        }

        @Override
        public byte[] prepareConnectionData(URLConnection connection) throws IOException {
            MultiPartForm mpf = new MultiPartForm(encoding);

            for (int i = 0; i < requestParams.length; i += 2) {
                mpf.addPart(requestParams[i], null, null, requestParams[i + 1].getBytes());
            }

            final byte[] data = mpf.toByteArray();
            connection.setRequestProperty("Content-type", "multipart/form-data; boundary=" + mpf.getBoundary());

            return data;
        }
    }

    /**
     * Abstract class for different types request tasks
     */
    private abstract class RequestTask implements Runnable {

        final String url;
        final String encoding;
        final String[] requestParams;
        final String userAgent;
        final JSObject deferred;

        public RequestTask(String url, String encoding, String[] requestParams, String userAgent, JSObject deferred) {
            this.url = url;
            this.encoding = encoding;
            this.requestParams = requestParams;
            this.userAgent = userAgent;
            this.deferred = deferred;
        }

        public abstract byte[] prepareConnectionData(URLConnection connection) throws IOException;

        protected HttpURLConnection prepareConnection(final String url) throws IOException {
            final URL base = getDocumentBase();
            final URL postURL = new URL(base, url);

            deferred.call("notify", new String[]{"init"});

            return (HttpURLConnection) postURL.openConnection();
        }

        private String[] execute() throws IOException, MalformedURLException {
            deferred.call("notify", new String[]{"init"});

            final HttpURLConnection connection = prepareConnection(url);
            connection.setDoOutput(true);

            final byte[] data = prepareConnectionData(connection);

            //JRA-24166: Screenshot applet now tells the server to ignore XSRF check in case the token
            //           is wrong. Which seems to be possible for multiple reasons.
            connection.setRequestProperty("X-Atlassian-Token", "no-check");

            if (sessionId != null) {
                connection.setRequestProperty("Cookie", String.format("%s=%s", JSESSIONID, sessionId));
            }

            if ((userAgent == null) || userAgent.equals("")) {
                System.out.println("WARNING: User-Agent unknown, user will be logged out");
            } else {
                connection.setRequestProperty("User-Agent", userAgent);
            }

            final Exception connectionException = AccessController.<Exception>doPrivileged(
                    new PrivilegedAction<Exception>() {
                        public Exception run() {
                            OutputStream os = null;
                            try {
                                os = connection.getOutputStream();
                                os.write(data);
                            } catch (Exception e) {
                                return e;
                            } finally {
                                if (os != null) {
                                    try {
                                        os.close();
                                    } catch (IOException e) {
                                    }
                                }
                            }
                            return null;
                        }
                    });

            if (connectionException != null) {
                throw new IOException(connectionException);
            } else {
                final List<String> responseHeaders = getResponseHeaders(connection);

                try
                {
                    final String result = readResult(connection.getInputStream());

                    deferred.call("notify", new Integer[] { 100 });

                    List<String> resolveArgs = new ArrayList<String>();
                    resolveArgs.add(result);
                    resolveArgs.addAll(responseHeaders);

                    return resolveArgs.toArray(new String[resolveArgs.size()]);
                }
                catch(IOException e)
                {
                    throw new IOException(readResult(connection.getErrorStream()), e);
                }
            }
        }

        private String readResult(InputStream inputStream) throws IOException {
            final StringBuilder result = new StringBuilder();

            final BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

            if (in != null) {
                try {
                    String inputLine;
                    while ((inputLine = in.readLine()) != null) {
                        result.append((result.length() == 0 ? "" : "\n") + inputLine);
                    }
                } catch (Exception e) {
                    throw new IOException(e);
                } finally {
                    in.close();
                }
            } else {
                throw new IOException("InputStream is null");
            }

            return result.toString();
        }

        /**
         * Return flat list of key-values, because we can't return map via LiveConnect
         * @param connection
         * @return List<String></String>
         */
        private List<String> getResponseHeaders(URLConnection connection) {
            final List<String> responseHeaders = new ArrayList<String>();
            for (Map.Entry<String, List<String>> headerField : connection.getHeaderFields().entrySet()) {
                String headerName = headerField.getKey();

                for (String headerValue : headerField.getValue()) {
                    responseHeaders.add(headerName);
                    responseHeaders.add(headerValue);
                }

                if ("Set-Cookie".equalsIgnoreCase(headerName)) {
                    for (String setCookie : headerField.getValue()) {
                        List<HttpCookie> httpCookies = HttpCookie.parse(setCookie);
                        for (HttpCookie httpCookie : httpCookies) {
                            if (JSESSIONID.equalsIgnoreCase(httpCookie.getName())) {
                                sessionId = httpCookie.getValue();
                            }
                        }
                    }
                }
            }
            return responseHeaders;
        }

        @Override
        public void run() {
            try {
                String[] result = execute();

                deferred.call("resolve", result);
            } catch (MalformedURLException e) {
                deferred.call("reject", new String[]{"Invalid URL"});
            } catch (IOException e) {
                e.printStackTrace();
                deferred.call("reject", new String[]{"Connection error", e.getMessage()});
            } catch (Exception e) {
                e.printStackTrace();
                deferred.call("reject", new String[]{"Unknown error"});
            }
        }
    }
}
