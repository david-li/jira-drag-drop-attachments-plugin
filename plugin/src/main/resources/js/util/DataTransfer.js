define("dndattachment/util/DataTransfer", ['jquery'], function ($) {
    return Class.extend({
        init: function(_dataTransfer) {
            this._dataTransfer = _dataTransfer;
        },

        getFiles: function() {
            var files = this._dataTransfer.files;
            var items = this._dataTransfer.items;

            if(items && items.length > 0) {
                if(_.any(items, function(item) { return !!item.webkitGetAsEntry; })) {
                    return this.readEntries(items);
                }
            }

            if(files && files.length > 0) {
                return this.readFiles(this._dataTransfer.files)
            } else {
                return new $.Deferred().reject();
            }
        },

        readFiles: function(files) {
            var result = new $.Deferred();

            $.when.apply(window, _.map(files, function(file) {
                var deferred = new $.Deferred();

                var noExtension = !file.name.match(/\.([a-z0-9]+)$/i);

                // if there is no extension, we rely on a fact that underlying implementation
                // will throw permission denied on fopen call for directory path
                // typical candidates for directory are paths without extension at the end
                // and also file.size 8192, 4096 and below 1024(Mac OS X)
                if(noExtension && (file.size <= 8192) || file.size == 8192 || file.size == 4096 || file.size <= 1024) {
                    this.readFileAsText(file).fail(function() {
                        files = _(files).without(file);
                    }).always(deferred.resolve.bind(deferred));
                } else {
                    deferred.resolve();
                }

                return deferred;
            }.bind(this))).always(function() {
                result.resolve(files);
            });

            return result;
        },

        readFileAsText: function(file) {
            var result = new $.Deferred();

            var reader = new FileReader();
            reader.onload = function() {
                result.resolve(this.result);
            };
            reader.onerror = function() {
                result.reject(this.error);
            };

            reader.readAsText(file);

            return result;
        },

        readEntries: function(items) {
            var result = new $.Deferred();

            // if dropped item is a file or a directory, item.kind === "file"
            items = _.filter(items, function(item){
                return item.kind === "file";
            });

            $.when.apply(window, _.map(items, function (item) {
                return this.readEntry(item.webkitGetAsEntry(), item);
            }.bind(this))).then(function() {
                result.resolve(_.union.apply(_, arguments));
            }, result.reject.bind(result));

            return result;
        },

        readEntry: function(entry, item) {
            var result = new $.Deferred();

            if(entry.isFile) {
                if(item && item.getAsFile) {
                    result.resolve([item.getAsFile()]);
                } else {
                    entry.file(function(file) {
                        result.resolve([file]);
                    });
                }
            } else if(entry.isDirectory) {
                entry.createReader().readEntries(function(entries) {
                    var files = [];

                    $.when.apply(window, _.map(entries, function(entry) {
                        return this.readEntry(entry).then(function(entryFiles) {
                            return files.push.apply(files, entryFiles);
                        });
                    }.bind(this))).always(function() {
                        result.resolve(files);
                    });
                }.bind(this));
            }

            return result;
        }
    })
});