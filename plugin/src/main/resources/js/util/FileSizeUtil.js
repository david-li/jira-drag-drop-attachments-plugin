/**
 * Ported from com.atlassian.core.util.FileSize
 */

define('dndattachment/util/FileSizeUtil',
function() {
    var KB_SIZE = 1024;
    var MB_SIZE = KB_SIZE * KB_SIZE;

    var KB = " kB";
    var MB = " MB";

    /**
     * Format the size of a file in human readable form.  Anything less than a kilobyte
     * is presented in kilobytes to one decimal place.  Anything between a kilobyte and a megabyte is
     * presented in kilobytes to zero decimal places.  Anything greater than one megabyte is
     * presented in megabytes to two decimal places.
     * <p>
     * eg.
     * <ul>
     *  <li>format(512) -> 0.5 kb
     *  <li>format(1024) -> 1.0 kb
     *  <li>format(2048) -> 2 kb
     *  <li>format(1024 * 400) -> 400 kb
     *  <li>format(1024 * 1024) -> 1024 kb
     *  <li>format(1024 * 1024 * 1.2) -> 1.20 Mb
     *  <li>format(1024 * 1024 * 20) -> 20.00 Mb
     * </ul>
     *
     * @param   filesize  The size of the file in bytes.
     * @return  The size in human readable form.
     */
    function format(filesize) {
        // TODO: filesize = 1024 gives "1.0 kB", but filesize = 1025 gives "1 kB", this is kinda inconsistent.

        if (filesize > MB_SIZE)
        {
            return formatMB(filesize);
        }
        else if (filesize > KB_SIZE)
        {
            return formatKB(filesize);
        }
        else
        {
            return formatBytes(filesize);
        }

    }

    function formatMB(filesize) {
        var mbsize = filesize / MB_SIZE;
        return mbsize.toFixed(2) + MB;
    }

    function formatKB(filesize) {
        var kbsize = Math.round(filesize / KB_SIZE); //format 0 decimal places
        return kbsize + KB;
    }

    function formatBytes(filesize) {
        var mbsize = filesize / KB_SIZE;
        return mbsize.toFixed(1) + KB;
    }

    return {
        format: format
    }
});