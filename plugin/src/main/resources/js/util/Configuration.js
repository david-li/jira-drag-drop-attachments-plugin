define("dndattachment/util/Configuration", function() {
    var dataKey = "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:dnd-issue-drop-zone.";

    function getWRMData(key) {
        return _.isFunction(WRM.data) ? WRM.data(key) : WRM.data.claim(key);
    }

    return {
        getWRM: function(key) {
            return getWRMData(dataKey + key);
        }
    }
});