require(['dndattachment/Parser',
        'jquery',
        'dndattachment/dropzones/IssueDropZone',
        'dndattachment/upload/handler',
        'dndattachment/upload/default/executor',
        'dndattachment/aui',
        'dndattachment/TemporaryAttachments',
        'dndattachment/util/Configuration',
        'dndattachment/util/FileSizeUtil'],
    function (Parser,
              $,
              IssueDropZone,
              uploadHandler,
              defaultUploadExecutor,
              AJS,
              Attachments,
              Config,
              FileSizeUtil) {
        if (!IssueDropZone.prototype.isSupportedBrowser()) {
            return;
        }

        var uploadLimit = Config.getWRM("upload-limit");
        var attachmentDropzone;

        function createDropZoneInContext($context, duiType) {
            var $fileInputList = $context.find('.field-group.file-input-list');

            if ($fileInputList.size() == 0) {
                return;
            }

            // skip if we already have dropzone there
            if ($fileInputList.find('*[duiType*="' + duiType + '"]').length > 0) {
                return;
            }

            var attachments = $fileInputList.find('input[name=filetoconvert]:checked').map(function (idx, el) {
                return Attachments.getAttachment(el.value, $(el).siblings('label').text());
            }).toArray();

            var description = $fileInputList.find('.description:last-child').html();

            // clear contents
            $fileInputList.empty();

            var $dropZone = $(JIRA.Templates.DnDAttachmentPlugin[duiType]({
                uploadLimit: FileSizeUtil.format(uploadLimit),
                jiraAttachmentSize: uploadLimit,
                description: description
            }));

            Parser.parse($dropZone.appendTo($fileInputList)).then(function (dropZone) {
                if (dropZone != null)
                    dropZone.loadAttachments(attachments);
            });
        }

        function createDropZoneInDialog(dialog, duiType) {
            var createDropZone = function () {
                createDropZoneInContext(dialog.$popupContent, duiType);
            };
            if (dialog.$popup && dialog.$popup.is(':visible')) {
                createDropZone();
            } else {
                dialog.onContentReady(createDropZone);
            }
        }

        function createAttachmentsDropZone($context) {
            var $attachmentModule = $context && ($context.attr("id") == "attachmentmodule") && $('.mod-content:not(.issue-drop-zone)', $context);

            if ($attachmentModule && $attachmentModule.size() > 0) {
                if ($context.find('#add-attachments-link').length == 0) {
                    // add attachment link is not there, means user don't have permissions
                    // This attachment link is hidden by this plugin and defined in JIRA
                    return;
                }

                $attachmentModule.addClass('issue-drop-zone');

                var $dropZone = $(JIRA.Templates.DnDAttachmentPlugin.AttachmentsDropZone({
                    uploadLimit: FileSizeUtil.format(uploadLimit),
                    jiraAttachmentSize: uploadLimit
                }));

                Parser.parse($dropZone.prependTo($attachmentModule)).then(function (dropZone) {
                    if (attachmentDropzone) {
                        attachmentDropzone.disconnectContainer();
                    }
                    attachmentDropzone = dropZone;
                    uploadHandler.setAttachmentDropZone(dropZone);
                });
            }
        }

        // Requiring Parser is enough to process all duiType declarations
        // besides that we need to install issue-drop-zone in attachFile and createIssue dialogs
        // each time when they become visible.
        // This is NOT supposed to inject drop zone everywhere.
        var onReady = function () {
            if (JIRA.Dialogs.attachFile) {
                createDropZoneInDialog(JIRA.Dialogs.attachFile, 'AttachFilesDropZone');
            }

            var triggerDialogs = ['create-issue-dialog', 'create-subtask-dialog', 'edit-issue-dialog'];

            var onDialogShow = function (event, $popupContent, dialog) {
                if (_.contains(triggerDialogs, $popupContent.attr('id'))) {
                    createDropZoneInDialog(dialog, 'CreateIssueDropZone');
                }
            };
            $(AJS).on('Dialog.show', onDialogShow);

            // just in case init file was loaded after Dialog.show event
            if (JIRA.Dialog.current) {
                onDialogShow(null, JIRA.Dialog.current.$popup, JIRA.Dialog.current);
            }

            JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function (e, context, reason) {
                createDropZoneInContext(context, 'CreateIssueDropZone');

                createAttachmentsDropZone(context);
            });

            var $createIssueForm = $('form#issue-create, form#issue-edit');
            if ($createIssueForm.size() > 0) {
                createDropZoneInContext($createIssueForm, 'CreateIssueDropZone');
            }

            uploadHandler.initialize();
            uploadHandler.registerExecutor(defaultUploadExecutor);
            createAttachmentsDropZone($('#attachmentmodule'));

            var DRAGOVER_CLASS = 'issue-drop-zone-document__dragover';

            $(document).on("dragover dragenter", function () {
                $('body').addClass(DRAGOVER_CLASS);
            });

            $(document).on("dragleave drop dropHandled", function () {
                $('body').removeClass(DRAGOVER_CLASS);
            });
        };

        if ($.isReady) {
            onReady();
        } else {
            AJS.$(onReady);
        }

        // we want dataTransfer property from originalEvent
        $.event.props.push("dataTransfer");
    });