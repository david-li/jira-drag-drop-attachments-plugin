define('dndattachment/Parser',
    ['dndattachment/aui',
     'jquery'],
function(AJS, $) {
    var PARSED_MARKCLASS = '-dui-type-parsed';
    var contextSelector = '*[duiType]:not(.'+PARSED_MARKCLASS+')';

    function parse(context) {
        var queue = $(context).find(contextSelector).add(context.filter(contextSelector));

        var deferreds = queue.addClass(PARSED_MARKCLASS).map(function(idx, el) {
            var result = new $.Deferred();

            require([$(el).attr('duiType')], function(duiType) {
                if(typeof duiType == "function")
                    result.resolve(new duiType(el));
                else
                    result.reject();
            });

            return result;
        });

        return $.when.apply(window, $.makeArray(deferreds));
    }

    JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, function(e, context, reason) {
        parse(context);
    });

    if($.isReady) {
        parse($('body'));
    } else {
        $(function() {
            parse($('body'));
        });
    }

    return {
        parse: parse
    };
});