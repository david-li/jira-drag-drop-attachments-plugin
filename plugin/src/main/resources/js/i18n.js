// because almond.js breaks jsI18n transformers

define('dndattachment/i18n', function() {
    var dict = {
        "dnd.attachment.unsupported.browser": AJS.I18n.getText("dnd.attachment.unsupported.browser"),
        "dnd.attachment.file.is.too.large": AJS.I18n.getText("dnd.attachment.file.is.too.large"),
        "dnd.attachment.upload.in.progress": AJS.I18n.getText("dnd.attachment.upload.in.progress"),
        "dnd.attachment.not.uploaded": function(param) {
            return AJS.I18n.getText("dnd.attachment.not.uploaded", param)
        },
        "dnd.attachment.unauthorized": AJS.I18n.getText("dnd.attachment.unauthorized"),
        "dnd.attachment.internal.server.error": AJS.I18n.getText("dnd.attachment.internal.server.error"),
        "dnd.attachment.upload.aborted": AJS.I18n.getText("dnd.attachment.upload.aborted"),
        "dnd.attachment.upload.remove": AJS.I18n.getText("dnd.attachment.upload.remove")

    };
    return function(key) {
        if(dict[key])
            return dict[key];
        else
            throw "Unknown key: "+key;
    };
});