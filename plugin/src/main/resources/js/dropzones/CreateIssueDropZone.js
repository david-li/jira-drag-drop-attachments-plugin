define('dndattachment/dropzones/CreateIssueDropZone',
    ['jquery',
     'dndattachment/dropzones/FormDropZone',
     'dndattachment/progressbars/UploadProgressBar'],

    function($, FormDropZone, UploadProgressBar) {

        var CreateIssueDropZone = FormDropZone.extend({

            eventGroup: 'createissuedropzone',

            progressBarType: 'dndattachment/progressbars/ProjectUploadProgressBar',

            getContainerNode: function() {
                return $(this.$node.parents('.jira-dialog-content')[0] ||
                    this.$node.parents('form#issue-create').parents('.aui-page-panel-content')[0])
            },

            handleNewProgressBar: function (progressBar) {
                this._super.apply(this, arguments);
                progressBar.bind('onDestroy onFinished', _.bind(function() {
                    // Each time a progress bar is removed/finishes uploading, check if there are any more are uploading.
                    // If not, then remove any form errors (the only form errors are from files uploading).
                    if (!this.filesStillUploading()) {
                        var errors = {};
                        errors[this.$inputNode.attr("name")] = "";
                        JIRA.applyErrorsToForm(this.$formNode, errors);
                    }
                }, this));
            }
        });

        return CreateIssueDropZone;
    });