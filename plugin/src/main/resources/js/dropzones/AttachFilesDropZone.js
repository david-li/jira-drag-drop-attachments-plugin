define('dndattachment/dropzones/AttachFilesDropZone',
    ['dndattachment/dropzones/FormDropZone', 'jquery'],

function(FormDropZone, $) {

    var AttachFileDropZone = FormDropZone.extend({

        eventGroup: 'attachfiledropzone',

        init: function() {
            this._super.apply(this, arguments);

            // this will mark drop zone as dirty as long as all progress are not removed
            // no matter if uploading is finished or not
            this.bind('progressBarInserted', function(event, progressBar) {
                var uploadPending = new $.Deferred();
                this.queueTask(uploadPending);
                progressBar.result.done(uploadPending.resolve.bind(uploadPending));
                progressBar.bind('onBeforeDestroy', function() {
                    uploadPending.resolve();
                });
            }.bind(this));
        },

        checkMarkDirty: function() {
            // all tasks are done, succesful or not
            if(this.pendingQueue.length <= 1)
                this.markDirty(false);
        }
    });

    return AttachFileDropZone;
});