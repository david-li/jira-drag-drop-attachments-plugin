/**
 * The CommentDialogDropZone has been created initially to be used by Service Desk in it's agent attachments dialog.
 * It can be instantiated without using the Parser (as in initIssueDropZone.js) but the uploadLimit must be passed into
 * the constructor.
 */
define('dndattachment/dropzones/CommentDialogDropZone',
    ['dndattachment/Parser',
        'dndattachment/dropzones/FormDropZone',
        'jquery',
        'dndattachment/aui',
        'dndattachment/i18n',
        'dndattachment/JIRA',
        'dndattachment/util/DataTransfer',
        'dndattachment/util/FileSizeUtil'],

    function(Parser, FormDropZone, $, AJS, I18n, JIRA, DataTransfer, FileSizeUtil) {

        var CommentDialogDropZone = FormDropZone.extend({

            eventGroup: 'commentdialogdropzone',

            progressBarType: 'dndattachment/progressbars/TempUploadProgressBar',

            init: function(element, uploadLimit) {
                this._super.apply(this, arguments);
                if (uploadLimit) {
                    this.uploadLimit = uploadLimit;
                    this.uploadSize = FileSizeUtil.format(uploadLimit);
                }
            },

            render: function() {
                this.$node.html(JIRA.Templates.DnDAttachmentPlugin.CommentDialogDropZoneContainer({}));
            },

            getContainerNode: function() {
                return $(this.$node.parents('.dialog2-content-container'));
            },

            placeUploadProgressBar: function($progressBar) {
                return $progressBar.prependTo(this.$node.find(".issue-drop-zone__target"));
            },

            /**
             * Attaches all files that have successfully temporarily uploaded and returns a promise for all uploads
             */
            attachUploadedFiles: function() {
                this.attachFileUrl = contextPath + '/rest/jddap/1.0/attachment';
                var fileIDs = _.map(
                    _.filter(this.progressBars, function(bar) {
                        return bar.isSuccessful();
                    }),
                    function(bar) {
                        return bar.getFileID();
                    }
                );
                return this.attachFile(fileIDs);
            },

            /**
             * Returns all filenames of successfully uploaded files
             */
            getFileNames: function() {
                return _.map(
                    _.filter(this.progressBars, function(bar) {
                        return bar.isSuccessful();
                    }),
                    function(bar) {
                        return bar.getFileName();
                    }
                );
            }
        });

        return CommentDialogDropZone;
    });