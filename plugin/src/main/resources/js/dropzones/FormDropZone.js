define('dndattachment/dropzones/FormDropZone',
    ['dndattachment/Parser',
    'dndattachment/dropzones/IssueDropZone',
    'jquery',
    'dndattachment/aui',
    'dndattachment/i18n',
    'dndattachment/JIRA',
    'dndattachment/util/DataTransfer',
    'dndattachment/ctrlv/utility',
    'jira/util/events'],

    function(Parser,
             IssueDropZone,
             $,
             AJS,
             I18n,
             JIRA,
             DataTransfer,
             Utility,
             Events) {

        var FormDropZone = IssueDropZone.extend({

            eventGroup: 'formdropzone',

            init: function() {
                this._super.apply(this, arguments);
                this.progressBars = [];

                this.bind('uploadFinished', this.onUploadFinished.bind(this));

                this.$containerNode = this.getContainerNode();
                this.$formNode = this.$node.parents('form');
                this.formToken = this.$formNode.find('input[name="formToken"]').attr('value');

                // this is required by JIRA.applyErrorsToForm
                this.$inputNode = $('<input name="dnd-dropzone" type="hidden" />').insertAfter(this.$node);

                this.$fieldNode = this.$node.parents('.field-group');
                this.$fieldNode.addClass('long-field');

                this.connectContainer();
            },

            onUploadFinished: function(event, progressBar) {
                var $checkbox = $('<input type="checkbox" name="filetoconvert"/>')
                    .addClass("upload-progress-bar__checkbox")
                    .attr("value", progressBar.fileID)
                    .attr("id", "filetoconvert" + "-" + progressBar.fileID)
                    .prop("checked", true)
                    .insertBefore(progressBar.$node);

                progressBar.bind('onBeforeDestroy', function() {
                    $checkbox.prop("checked", false).prop("value", null);
                });

                setTimeout(function() {
                    progressBar.$node.addClass('upload-progress-bar__upload-ready');
                    progressBar.setIcon('aui-iconfont-close-dialog');
                }, 1000);

                JIRA.trace('jira.issue.dnd.uploadfinished');
            },

            commitUpload: function(fileIDs) {
                this.queueEvent('commitUpload', { count: fileIDs.length });
                // there is no auto commit in AttachFiles dialog
                // also it means that progress bars won't get destroyed
                return new $.Deferred();
            },

            getContainerNode: function() {
                return this.$node.parents('.jira-dialog-content');
            },

            connectContainer: function() {
                this.$dragoverMask = $('<div class="attach-files-drop-zone__dragover-mask" />');
                this.$dragoverMask.appendTo(this.$containerNode);
                this.$body = $(document.body);

                this.onDragOverDropHandler = this.onDragOverDrop.bind(this);
                this.onDragLeaveHandler = this.onDragLeave.bind(this);
                this.onBodyDragOverDropHandler = this.onBodyDragOverDrop.bind(this);
                this.$containerNode.on('dragover drop', this.onDragOverDropHandler);
                this.$containerNode.on('dragleave', this.onDragLeaveHandler);
                this.$body.on('dragover drop', this.onBodyDragOverDropHandler);

                // store handler so it can be removed(only this one)
                this.onBeforeSubmitHandler = this.onBeforeSubmit.bind(this);
                this.$formNode.on('before-submit', this.onBeforeSubmitHandler);

                this.onContentAddedHandler = this.onContentAdded.bind(this);
                JIRA.bind(JIRA.Events.NEW_CONTENT_ADDED, this.onContentAddedHandler);
                Events.bind("Dialog.hide", this.onDialogHide.bind(this));
                if (AJS.dialog2) {
                    AJS.dialog2.on("hide", this.onDialogHide.bind(this));
                }
            },

            disconnectContainer: function() {
                this.$dragoverMask.remove();
                this.$containerNode.removeClass('attach-files-drop-zone__dragover');

                this.$containerNode.off('dragover drop', this.onDragOverDropHandler);
                this.$containerNode.off('dragleave', this.onDragLeaveHandler);

                this.$formNode.off('before-submit', this.onBeforeSubmitHandler);
                JIRA.unbind(JIRA.Events.NEW_CONTENT_ADDED, this.onContentAddedHandler);
                Events.unbind("Dialog.hide", this.onDialogHide.bind(this));
                if (AJS.dialog2) {
                    AJS.dialog2.off("hide", this.onDialogHide.bind(this));
                }
            },

            isAttached: function() {
                return $.contains(document, this.$node[0]);
            },

            onContentAdded: function() {
                if(!this.isAttached()) {
                    this.disconnectContainer();
                }
            },

            onDialogHide: function () {
                this.$body.off('dragover drop', this.onBodyDragOverDropHandler);
            },

            onBodyDragOverDrop: function (event) {
                event.preventDefault();
                if (event.originalEvent) {
                    event.originalEvent.dataTransfer.dropEffect = "none";
                }
            },

            onDragOverDrop: function(event) {
                if (!this.isAttached()) {
                    this.disconnectContainer();
                    return;
                }
                if (!Utility.dragEventContainsFiles(event)) {
                    return;
                }

                event.preventDefault();
                event.stopPropagation();
                if (event.originalEvent) {
                    event.originalEvent.dataTransfer.dropEffect = "copy";
                }
                this.$containerNode.addClass('attach-files-drop-zone__dragover');

                if(event.type == 'drop') {
                    this.$containerNode.removeClass('attach-files-drop-zone__dragover');
                    $(document).trigger("dropHandled");

                    var dataTransfer = new DataTransfer(event.dataTransfer);
                    dataTransfer.getFiles().then(function(files) {
                        this.handleFilesReceived(files);
                        this.queueEvent('fileDrop', { count: files.length });
                    }.bind(this));
                }
            },

            onDragLeave: function() {
                this.$containerNode.removeClass('attach-files-drop-zone__dragover');
            },

            onBeforeSubmit: function(event) {
                if(this.isDirty) {
                    event.preventDefault();
                    var errors = {};
                    errors[this.$inputNode.attr("name")] = I18n("dnd.attachment.upload.in.progress");

                    JIRA.applyErrorsToForm(this.$formNode, errors);
                    this.$formNode.find('.error').toArray().some(function(el) {
                        el.scrollIntoView()
                    });
                }
            },

            markDirty: function(isDirty) {
                IssueDropZone.prototype.markDirty.apply(this, arguments);

                this.isDirty = isDirty;

                JIRA.trace(isDirty ? 'jira.issue.dnd.isdirty' : 'jira.issue.dnd.isclear');
            },

            /**
             * We want to keep track of all the progress bars in our drop zone
             */
            handleNewProgressBar: function(progressBar) {
                this.progressBars.push(progressBar);

                // Remove from our list of progress bars if it's destroyed
                progressBar.bind('onBeforeDestroy', _.bind(function() {
                    this.trigger("progressBarOnBeforeDestroy", progressBar);
                    this.progressBars = _.filter(this.progressBars, function(bar) {
                        return bar !== progressBar;
                    });
                    if (this.progressBars.length === 0) {
                        this.trigger("allUploadsCancelled");
                    }
                }, this));

                progressBar.bind('onDestroy', _.bind(function() {
                    this.trigger('progressBarDestroyed');
                }, this));

                progressBar.bind('onFinished', _.bind(function() {
                    this.trigger("progressBarFinished");
                }, this));
                progressBar.bind('onFailed', _.bind(function() {
                    this.trigger("progressBarFailed");
                }, this));
                this.trigger("progressBarStarted");
            },

            /**
             * Returns true if any files are still uploading
             */
            filesStillUploading: function() {
                return _.filter(this.progressBars, function (bar) {
                        return !bar.isFinished();
                    }).length > 0;
            },

            /**
             * Returns true if any files failed to upload
             */
            anyFilesFailed: function() {
                return _.filter(this.progressBars, function(bar) {
                        return bar.isFailed();
                    }).length > 0;
            }
        });

        return FormDropZone;
    });