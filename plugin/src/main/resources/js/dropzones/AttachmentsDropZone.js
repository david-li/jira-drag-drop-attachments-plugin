define('dndattachment/dropzones/AttachmentsDropZone', [
        'dndattachment/dropzones/IssueDropZone',
        'jira/util/events',
        'dndattachment/util/events/types',
        'jquery',
        'dndattachment/util/DataTransfer',
        "dndattachment/ctrlv/utility",
        "jira/dialog/dialog"
    ],
    function (IssueDropZone,
              Events,
              EventTypes,
              $,
              DataTransfer,
              Utility,
              JIRADialog) {

        var canonicalBaseUrl = (function () {
            var uri = parseUri(window.location);
            return uri.protocol + "://" + uri.authority;
        })();

        var AttachmentsDropZone = IssueDropZone.extend({
            eventGroup: 'attachmentsdropzone',
            progressBarType: 'dndattachment/progressbars/AttachmentsUploadProgressBar',

            init: function () {
                this._super.apply(this, arguments);

                this.attachFileUrl = contextPath + '/rest/jddap/1.0/attachment';
                Events.bind(JIRA.Events.PANEL_REFRESHED, this.panelRefreshed.bind(this));

                this.connectContainer();
            },

            connectContainer: function() {
                this.$pagePanel = $("div.aui-page-panel");
                this.$containerNode = this.getContainerNode();
                this.$dragoverDropzone = $(JIRA.Templates.DnDAttachmentPlugin.dropzone());
                this.$dragoverBorder = this.$dragoverDropzone.find(".attachments-drop-zone__dragover-border");
                this.$dragoverInfo = this.$dragoverDropzone.find(".attachments-drop-zone__dragover-info");
                this.$dragoverDropzone.appendTo(this.$containerNode);
                this.onDragOverDropHandler = this.onDragOverDrop.bind(this);
                this.onDragLeaveHandler = this.onDragLeave.bind(this);
                this.$containerNode.on('dragover dragenter drop', this.onDragOverDropHandler);
                this.$containerNode.on('dragleave', this.onDragLeaveHandler);

                this.$windowMask = $(JIRA.Templates.DnDAttachmentPlugin.windowDropzoneMask());
                this.$body = $(document.body);
                this.$body.append(this.$windowMask);
                this.$body.on('dragenter', _.throttle(this.onBodyDragEnter.bind(this), 200, {trailing: false}));
                this.$body.on('dragleave', this.onBodyDragLeave.bind(this));
                this.$windowMask.on('dragover dragenter', this.onWindowMaskDragEnter.bind(this));
                this.$windowMask.on('dragleave', this.onWindowMaskDragLeave.bind(this));
                this.$windowMask.on('drop', this.onWindowMaskDrop.bind(this));

                // Save the aui sidebar for easy access
                this.$auiSidebar = $(".aui-sidebar-body");

                // Disconnect the listeners
                this._disconnectListenersFromParentDropzone();

                this.onContentAddedHandler = this.onContentAdded.bind(this);
                Events.bind(JIRA.Events.NEW_CONTENT_ADDED, this.onContentAddedHandler);
            },

            _disconnectListenersFromParentDropzone: function() {
                // Remove the default event handlers from IssueDropZone since we are overriding it.
                this.$dropTarget.off('dragover drop dragleave dragenter');
                $(document).off('dragover dragenter');
            },

            // dynamically changes z-index for aui-page-panel to workaround this: https://bulldog.internal.atlassian.com/browse/JSEV-510
            _risePagePanel: function() {
                this._pagePanelZIndex = this.$pagePanel.css("z-index");
                this.$pagePanel.css("z-index", "auto");
            },

            // returns aui-page-panel z-index to previous state after _risePagePanel
            // due to https://bulldog.internal.atlassian.com/browse/JSEV-510
            _releasePagePanel: function() {
                this.$pagePanel.css("z-index", this._pagePanelZIndex);
            },

            disconnectContainer: function() {
                this.$dragoverDropzone.remove();
                this.$windowMask.remove();
                this.$body.removeClass('attachments-drop-zone__dragover');

                this.$body.off('dragenter', this.onBodyDragEnter.bind(this));
                this.$containerNode.off('dragenter dragover drop', this.onDragOverDropHandler);
                this.$containerNode.off('dragleave', this.onDragLeaveHandler);

                Events.unbind(JIRA.Events.NEW_CONTENT_ADDED, this.onContentAddedHandler);
            },

            isAttached: function() {
                return $.contains(document, this.$node[0]);
            },

            onContentAdded: function() {
                if(!this.isAttached()) {
                    this.disconnectContainer();
                }
            },

            getContainerNode: function() {
                var $node = $($(".issue-container")[0]);
                // In the case of the issue nav view, we need to modify the parent to have a relative position so that
                // our dragover border is positioned correctly as it's child.
                if ($node.css('position') === 'static') {
                    $node.css('position', 'relative');
                }
                return $node;
            },

            /**
             * We don't want to do a full page drag+drop if they are editing a field or have a dialog open.
             * This will change in the future (they will soon be allowed to drag+drop into wiki-renderable fields).
             * @returns {boolean}
             */
            canDoFullPageDrop: function() {
                if (!JIRADialog.current) {
                    JIRA.trace("jira.issue.dnd.dropallowed");
                    return true;
                } else {
                    JIRA.trace("jira.issue.dnd.dropnotallowed");
                    return false;
                }
            },

            onDragOverDrop: function(event) {
                if (!this.isAttached()) {
                    this.disconnectContainer();
                    return;
                }
                if (!Utility.dragEventContainsFiles(event)) {
                    return;
                }
                if (!this.canDoFullPageDrop()) {
                    return;
                }
                event.preventDefault();
                if (event.originalEvent) {
                    event.originalEvent.dataTransfer.dropEffect = "copy";
                }

                JIRA.trace('jira.issue.dnd.dragover');
                this.$body.addClass('attachments-drop-zone__dragover');
                this.$body.addClass("attachments-drop-zone__window-dragover");
                this.$containerNode.addClass("attachments-drop-zone__dragover-fade");

                this._readjustDropzone();
                this._repositionStickyInfoBox();
                if(event.type == 'drop') {
                    this.removeAllDragoverClasses(this.$containerNode);
                    this._releasePagePanel(); // https://bulldog.internal.atlassian.com/browse/JSEV-510
                    $(document).trigger("dropHandled");

                    var dataTransfer = new DataTransfer(event.dataTransfer);
                    dataTransfer.getFiles().then(function(files) {
                        this.handleFilesReceived(files);
                        this.queueEvent('fileDrop', { count: files.length });
                    }.bind(this));
                }
            },

            onDragLeave: function() {
                this.removeAllDragoverClasses(this.$containerNode);
            },

            /**
             * If the side-bar is in fly-out mode it overlays over the issue, so we need to adjust the dropzone in that case
             */
            _readjustDropzone: function() {
                if (this.$auiSidebar.length !== 0) {
                    var sidebarWidth = this.$auiSidebar.offset().left + this.$auiSidebar.width();
                    // IE can encounter problems with the dragoverBorder's parent offset, so check it's defined.
                    if (this.$dragoverBorder.parent().offset()) {
                        var dropzoneParentLeft = this.$dragoverBorder.parent().offset().left;
                        if (sidebarWidth > dropzoneParentLeft) {
                            this.$dragoverBorder.css({left: sidebarWidth - dropzoneParentLeft});
                        } else {
                            this.$dragoverBorder.css({left: 0});
                        }
                    } else {
                        this.$dragoverBorder.css({left: 0});
                    }
                    this._risePagePanel(); // https://bulldog.internal.atlassian.com/browse/JSEV-510
                }
            },

            _repositionStickyInfoBox: function() {
                // dragover-info should always be visible, so we calculate its position based on
                // how much the user has scrolled down within the containerNode and whether the
                // scrollParent of the containerNode has an offset.
                var scroll = this.$containerNode.scrollParent().scrollTop() + 10;
                var nodeOffset = this.$containerNode.offset().top;
                // Calling .offset() on $(document) returns null, so make sure we handle that case properly
                var parentOffset;
                if (this.$containerNode.scrollParent().offset()) {
                    parentOffset = this.$containerNode.scrollParent().offset().top;
                } else {
                    parentOffset = 0;
                }

                var dragoverInfoTop;

                if(parentOffset === 0){
                    if(scroll >= nodeOffset){
                        dragoverInfoTop = scroll - nodeOffset;
                    }
                    else{
                        dragoverInfoTop = 10;
                    }
                }
                else{
                    dragoverInfoTop = scroll;
                }

                this.$dragoverInfo.css({top: dragoverInfoTop});
            },

            onBodyDragEnter: function(e) {
                if (!Utility.dragEventContainsFiles(e)) {
                    return;
                }
                this._readjustDropzone();
                this.$body.addClass("attachments-drop-zone__window-dragover");
            },

            onBodyDragLeave: function() {
                this.removeAllDragoverClasses();
            },

            onWindowMaskDragEnter: function(event) {
                if (!Utility.dragEventContainsFiles(event)) {
                    return;
                }
                event.preventDefault();
                if (event.originalEvent) {
                    event.originalEvent.dataTransfer.dropEffect = "none";
                }
                this._readjustDropzone();
                this.$body.addClass("attachments-drop-zone__window-dragover");
                if (this.canDoFullPageDrop()) {
                    this.$body.addClass("attachments-drop-zone__dragover");
                    this._repositionStickyInfoBox();
                }
            },

            onWindowMaskDragLeave: function() {
                this.removeAllDragoverClasses();
            },

            onWindowMaskDrop: function(event) {
                event.preventDefault();
                this.removeAllDragoverClasses();
            },

            removeAllDragoverClasses: function(fadeMask) {
                if (fadeMask) {
                    fadeMask.removeClass("attachments-drop-zone__dragover-fade")
                }
                this.$body.removeClass("attachments-drop-zone__window-dragover");
                this.$body.removeClass("attachments-drop-zone__dragover");
            },

            handleFilesReceived: function (files) {
                var wikiTextfield = $("textarea.wiki-textfield:visible:focus");
                var isWikiTextfieldFocused = wikiTextfield.length > 0;

                // Visual mode of jira editor
                var richEditorField = $(".jira-editor-container iframe:visible:focus");
                var isRichEditorFieldFocused = richEditorField.length > 0;

                // Files that are received in the drop zone should commit them to the page using the event handler so
                // they are handled in the same way as other attachments that are committed directly to the page
                Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
                    files: files,
                    // If a wiki textfield, or a richeditor field is in focus we insert the markup.
                    // We want the attachment executor to know if a wiki textfield or a richeditor field is in focus
                    // so that the executor can then decide whether it wants to perform its action.
                    isWikiTextfieldFocused: isWikiTextfieldFocused,
                    wikiTextfield: wikiTextfield[0],
                    isRichEditorFieldFocused: isRichEditorFieldFocused,
                    richEditorField: richEditorField[0]
                });
            },

            render: function () {
                this.$node.parents("#attachmentmodule").addClass('attachments-drop-zone-parent');
                this.$listNode = this.$node.siblings('#file_attachments, #attachment_thumbnails');

                return this._super.apply(this, arguments);
            },

            panelRefreshed: function (event, panel, $new, $existing) {
                // our drop zone was attached to refreshed panel, so we should reattach it and recover removed progress bars
                if (!$existing.find(this.$node).is('*')) {
                    return;
                }

                var oldViewMode = this.getViewMode();

                $new = $new.find('>.mod-content');
                $new.addClass('issue-drop-zone');
                this.$node.prependTo($new);
                this.render();

                var $existingProgressBars = $existing.find('.attachments-upload-progress-bar');

                $existingProgressBars.data("viewMode", this.getViewMode()).each(function (idx, progressBar) {
                    $(progressBar).prop("_instance").render();
                });

                $existingProgressBars.each(function (idx, progressBar) {
                    this.insertProgressBar($(progressBar));
                }.bind(this));

                // Disconnect the listeners since they get reattached
                this._disconnectListenersFromParentDropzone();
            },

            /**
             *
             * @returns {string} "list" or "gallery"
             */
            getViewMode: function () {
                return this.$listNode.attr("id") == "file_attachments" ? "list" : "gallery";
            },

            /**
             *
             * @returns {string} "fileName" or "dateTime"
             */
            getSortBy: function () {
                return this.$listNode.data("sort-key");
            },

            /**
             *
             * @returns {string} "desc" or "asc"
             */
            getSortOrder: function () {
                return this.$listNode.data("sort-order");
            },

            /**
             * Return sort comparator function that takes two nodes
             * @returns {Function}
             */
            getSortComparator: function () {
                var sortOrder = this.getSortOrder() == "asc" ? 1 : -1;
                var getSortValue = this.getSortBy() == "fileName" ? function ($node) {
                    return $node.find('.attachment-title').text();
                } : function ($node) {
                    return new Date($node.find('*[datetime]').attr("datetime")).getTime();
                };

                var compareFn = this.getSortBy() == "fileName" && String.prototype.localeCompare ?
                    function (left, right) {
                        return left.localeCompare(right);
                    } :
                    function (left, right) {
                        return left < right ? -1 : (left > right ? 1 : 0);
                    };

                return function ($nodeLeft, $nodeRight) {
                    var left = getSortValue($nodeLeft);
                    var right = getSortValue($nodeRight);
                    return sortOrder * compareFn(left, right);
                }
            },

            commitUpload: function (fileIDs) {
                var result = new $.Deferred();

                this.attachFile(fileIDs).then(result.resolve.bind(result), result.reject.bind(result));

                result.then(function (beans) {
                    var $beans = $(JIRA.Templates.ViewIssue.renderAttachments({
                        baseurl: contextPath,
                        fullBaseUrl: canonicalBaseUrl + contextPath,
                        issue: {id: JIRA.Issues.Api.getSelectedIssueId()},
                        attachments: beans,
                        viewMode: this.getViewMode()
                    })).find('.attachment-content');

                    beans.forEach(function (bean, idx) {
                        var beanNode = $beans[idx];
                        var $progressBar = this.$listNode.find('>*[data-file-id="' + fileIDs[idx] + '"]');
                        $progressBar.replaceWith(beanNode);
                    }, this);

                    JIRA.trigger(JIRA.Events.NEW_CONTENT_ADDED, [this.$listNode, JIRA.CONTENT_ADDED_REASON.panelRefreshed]);

                    JIRA.trace('jira.issue.dnd.attached');
                }.bind(this));

                this.queueTask(result);

                this.queueEvent('commitUpload', {count: fileIDs.length});

                return result;
            },

            configureUploadProgressBar: function ($progressBar) {
                this._super.apply(this, arguments);

                $progressBar.data("viewMode", this.getViewMode());
            },

            placeUploadProgressBar: function ($progressBar, progressBar) {
                progressBar.$node.find('time.livestamp').attr("datetime", new Date().toISOString()).livestamp();

                this.insertProgressBar($progressBar);

                progressBar.bind("onDestroy", function () {
                    var $itemAttachments = $progressBar.parents('.item-attachments');
                    if ($itemAttachments.find(".attachments-upload-progress-bar, .attachment-content").length == 0) {
                        $itemAttachments.remove();
                    }
                });

                return $progressBar;
            },

            /**
             * Insert given progressBar element on attachment list respecting sorting options
             * @param $progressBar
             */
            insertProgressBar: function ($progressBar) {
                var comparator = this.getSortComparator();

                var $children = this.$listNode.children().toArray();

                var isGreater = function (item) {
                    if (comparator($progressBar, $(item)) < 1) {
                        $progressBar.insertBefore(item);
                        return true;
                    }
                };

                if (!$children.some(isGreater)) {
                    $progressBar.appendTo(this.$listNode);
                }
            }

        });

        return AttachmentsDropZone;
    });