define('dndattachment/TemporaryAttachments', [], function() {
    var attachments = { };

    return {
        createAttachment: function(id, name, type, file) {
            return {
                id: id,
                name: name,
                type: type,
                file: file
            };
        },

        getAttachment: function(id, name) {
            return attachments[id] || this.createAttachment(id, name);
        },

        putAttachment: function(id, file) {
            return attachments[id] = this.createAttachment(id, file.name, file.type, file);
        }
    }
});