define("dndattachment/ctrlv/wrmdata", [
  "exports"
], function (
  exports
) {
  "use strict";
  exports.claim = function(key){
    window.WRM.data.claim(key);
  };
});
