define("dndattachment/ctrlv/version", [
    "jquery",
    "underscore",
    "jira/util/navigator",
    "exports"
  ],
  function(
    $,
    _,
    Navigator,
    exports
  ){
    "use strict";


    // It's a sad state of affairs that we need this browser detection, but unfortunately there's no reliable feature
    // detection for image pasting from clipboard support, so we're left with browser detection.
    //
    // JIRA politely puts classes on <html> to identify the browser for us, so let's treat that as our browser detection
    // API. We only care about IE for special casing clipboard behaviour, for all other browsers we'll assume they
    // support the HTML5 clipboard API,
    var classes = document.documentElement.className.split(/\s+/);
    var isIE = Navigator.isIE();
    var gt7 = $.inArray("msie-gt-7", classes) > -1;
    var gt8 = $.inArray("msie-gt-8", classes) > -1;
    var gt9 = $.inArray("msie-gt-9", classes) > -1;
    var gt10 = $.inArray("msie-gt-10", classes) > -1;

    var isIE8  = isIE && gt7 && !gt8;
    var isIE9  = isIE && gt8 && !gt9;
    var isIE10 = isIE && gt9 && !gt10;

    exports.isIE8 = _.once(function(){
      return isIE8;
    });

    exports.isIE9 = _.once(function(){
      return isIE9;
    });

    exports.isIE10 = _.once(function(){
      return isIE10;
    });

});
