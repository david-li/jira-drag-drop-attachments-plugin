define("dndattachment/ctrlv/resource-uris", [
    "jquery",
    "exports"
], function (
  $,
  exports
) {
    "use strict";

    var dataKey = "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources.resource-uris";
    var data = WRM.data.claim(dataKey) || {};

    exports.get = function(name) {
        return data[name];
    };
});