define("dndattachment/ctrlv/executor", [
    "jquery",
    "jira/util/events",
    "dndattachment/ctrlv/html5",
    "dndattachment/upload/handler",
    "exports"
], function (
    $,
    Events,
    html5,
    DnDUploadHandler,
    exports
) {

    exports.register = function () {
        /*
         * This executor spawns the attach screenshot dialog and is only valid if user pastes an attachment via ctrlv.
         */
        var ctrlvExecutor = {
            name: 'JIRA Ctrl+V attachment executor',
            // This weight is higher than the default executor (weight 0) and lower than the ServiceDesk executor (weight 50)
            weight: 5,
            isValid: function (event, args) {
                return !!args.isPaste;
            },
            processFiles: function (files, attachmentDropZone) {
                var deferred = $.Deferred();
                html5.show().done(function (dialog) {
                    var $el = $("#attach-screenshot-placeholder-message");
                    var evt = $.Event("paste");
                    evt.clipboardData = {files: files};
                    $el.focus();
                    setTimeout(function() {
                        $el.trigger(evt)
                    });

                    Events.bind("Dialog.hide", function (event, $popup, reason) {
                        if (reason) {
                            deferred.reject();
                        }
                        else {
                            // If there is no reason, then we know the file was uploaded successfully.
                            deferred.resolve([$popup.find('#attachscreenshotname').val() + '.png']);
                        }
                    });
                });

                return deferred;
            }
        };

        DnDUploadHandler.registerExecutor(ctrlvExecutor);
    }

});