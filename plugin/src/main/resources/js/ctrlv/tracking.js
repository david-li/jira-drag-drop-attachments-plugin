define("dndattachment/ctrlv/tracking", [
  "exports"
], function (
  exports
) {
  "use strict";
  exports.trigger = function (analyticKey, payload) {
    AJS.trigger("analytics", {name: analyticKey, data: payload || {}});
  };
});

