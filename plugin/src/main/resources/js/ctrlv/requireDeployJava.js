define("dndattachment/ctrlv/requireDeployJava", [
    "jquery",
    "dndattachment/ctrlv/resource-uris",
    "exports"
], function (
  $,
  resourceUris,
  exports
) {
    "use strict";

    var deployJavaPromise = null;

    /**
     * AMD-ish pattern akin to require('deployJava').
     *
     * Oracle provides a JavaScript function -- deployJava -- that makes it easy to install Java in the browser.
     * @returns a jQuery promise that's resolved with two arguments:
     *     - {Function} `deployJava`
     *     - {WindowProxy} the contentWindow where `deployJava` exists.
     */
    exports.requireDeployJava = function () {
        var deferred;

        if (deployJavaPromise !== null) {
            return deployJavaPromise;
        }

        deferred = $.Deferred();
        deployJavaPromise = deferred.promise();

        $('<iframe style="display: none !important; visibility: hidden !important; opacity: 0"/>')
            .attr('src', resourceUris.get("deployJava.html"))
            .one('load', function () {
                deferred.resolve(this.contentWindow.deployJava, this.contentWindow);
            })
            .one('error', deferred.reject.bind(deferred))
            .appendTo('body');

        return deployJavaPromise;
    };
});
