define("dndattachment/ctrlv/trace", [], function () {
  "use strict";
  // tests often redefine JIRA.trace this avoid grabbing a stale version
  return function amdJiraTrace() {
    return JIRA.trace.apply(undefined, arguments);
  };
});
