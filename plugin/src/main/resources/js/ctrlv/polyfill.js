define("dndattachment/ctrlv/polyfill", [
    "jquery",
    "underscore",
    "dndattachment/ctrlv/utility",
    "dndattachment/ctrlv/requireDeployJava",
    "dndattachment/ctrlv/resource-uris",
    "dndattachment/ctrlv/path",
    "dndattachment/ctrlv/version",
    "jira/ajs/ajax/smart-ajax",
    "exports"
], function (
  $,
  _,
  utility,
  requireDeployJava,
  resourceUris,
  path,
  version,
  SmartAjax,
  exports
) {
    "use strict";

    var appletNode;

    var pasteImageWidth = 510;
    var pasteImageHeight = 280;
    var dataURILimit = version.isIE8() ? 32000 : Number.MAX_VALUE;

    exports.pasteImageWidth = pasteImageWidth;
    exports.pasteImageHeight = pasteImageHeight;
    exports.dataURILimit = dataURILimit;

    /**
     * Create a rejected jQuery deferred. All arguments are passed to .reject().
     * @returns a jQuery deferred.
     */
    function fail() {
        var deferred = $.Deferred();
        return deferred.reject.apply(deferred, arguments);
    }

    var $node;

    // paste polyfill
    var handleKeydown = function (event) {
        if (utility.isKeyPasteEvent(event)) {
            appletNode.reload();
            var imageUri = appletNode.getClipboardData();
            if (imageUri) {
                var previewImageUri = appletNode.getClipboardData(pasteImageWidth, pasteImageHeight);
                if (previewImageUri.length > dataURILimit) {
                    var f = dataURILimit / previewImageUri.length;
                    previewImageUri = appletNode.getClipboardData(pasteImageWidth * f, pasteImageHeight * f);
                }
                $node.trigger('polyPaste', [imageUri, previewImageUri]);
            }
        }
    };

    /**
     * Use the Java applet to make a HTTP request.
     * @param data
     * @param requestUrl
     * @returns a jQuery deferred that pretends to be a jqXHR. The only addition is a '.abort()' method to try to
     *   conform closer to jqXHR API.
     */
    function executeAjaxUpload(data, requestUrl) {
        var cookies = document.cookies,
          deferred = $.Deferred();

        var xhr = {
            getResponseHeader: function (name) {
                return responseHeaders[name];
            }
        };

        var requestDeferred = new $.Deferred()
          .progress(_.bind(deferred.notify, deferred))
          .done(_.bind(deferred.resolve, deferred))
          .fail(function(error, responseJson) {
              var errorMessage = responseJson || JSON.stringify({ errorMessage: errorMessage });
              deferred.reject(null, null, null, new SmartAjax.SmartAjaxResult(xhr, 0, "error", errorMessage, true));
          });

        appletNode.doMultipartRequest(requestUrl, "UTF-8", window.navigator.userAgent, requestDeferred);
        // Java applets use the browser's cookie storage for URLConnection for non-http-only cookies. This means
        // xsrftoken and other cookies can pollute the browser's.
        deferred.always(function () {
            document.cookies = cookies;
        });
        return deferred;
    }

    /**
     * Install the polyfill to provide clipboard access to non-HTML5 browsers.
     *
     * @param {Element} node Where the 'keydown' event handler should be added.
     * @returns a jQuery promise that's resolved with [deployJava, contentWindow], or rejected
     *   with a [reason, message].
     *
     * Reasons include:
     * - "java-absent" -- Java isn't installed.
     * - "java-version" -- The version Java is not supported.
     * - "java-security" -- Java security settings are blocking the applet. Typically this can be overcome by
     *   lowering the 'Security Level' in Java's 'Control Panel' (e.g. from High to Medium).
     * - "unknown" -- Every other case.
     */
    exports.install = _.once(function (node) {
        $node = $(node);

        return requireDeployJava.requireDeployJava()
            .pipe(function (deployJava, contentWindow) {
                // Unfortunately using deployJava.versionCheck() isn't guaranteed to be accurate. Depending on the
                // platform and JRE. For example Java 1.7.0_06 on IE9 only reports the JRE as 1.7.0 (i.e. no update
                // information).
                // Given that the specific rules/cases/etc are ambiguous, we'll opt for doing a 'best effort' here
                // rather than hard coding a bunch of special case rules that aren't guaranteed to be reliable.
                var attributes,
                    parameters,
                    requiresLegacyApplet,
                    minimumVersion = '1.7.0_06',
                    uri;

                if (deployJava.getJREs().length === 0) {
                    return fail("java-absent");
                } else if (!deployJava.versionCheck(minimumVersion + '+')) {
                    return fail("java-version");
                }

                // Java 1.7.0_45 changed the manifest attributes that are required to allow JavaScript to invoke
                // applet methods. Java 1.7.0_40 and earlier requires the 'Trusted-Library' attribute, but later
                // versions require the 'Caller-Allowable-Codebase' attribute.
                //
                // See https://blogs.oracle.com/java-platform-group/entry/7u45_caller_allowable_codebase_and for more
                // details.
                requiresLegacyApplet = !deployJava.versionCheck('1.7.0_45+');
                uri = resourceUris.get(requiresLegacyApplet ? "clipboard-legacy.jar" : "clipboard.jar");

                attributes = {
                    id: 'JIRA HTML5 Images Applet',
                    codebase: path.dirname(uri),
                    code: "com.atlassian.plugins.jira.screenshot.applet.ScreenshotApplet.class",
                    archive: path.basename(uri),
                    width: 0,
                    height: 0
                };
                parameters = {
                    permissions: "all-permissions"
                };

                deployJava.runApplet(attributes, parameters, minimumVersion);
                appletNode = contentWindow.document.getElementById(attributes.id);

                try {
                    // We need a try/catch here because...
                    // A 'appletNode.isSecurity' doesn't work, because it's falsey on IE. We can use .hasOwnProperty
                    // but it doesn't tell us if it's a function, and 'typeof appletNode.isSecurity' returns
                    // "unknown".
                    if (!appletNode || !appletNode.isSecurityOk()) {
                        return fail("java-security");
                    }
                } catch (e) {
                    return fail("java-security");
                }

                return executeAjaxUpload;
            }, function () {
                return fail("unknown");
            })
            // Add a message to the error.
            .pipe(null, function (reason) {
                var messages = {
                    "unknown": AJS.I18n.getText("attach.screenshot.java.is.broken.unified"),
                    "java-version": AJS.I18n.getText("attach.screenshot.java.is.old.unified", "<a target=\"_blank\" href=\"//java.com\">", "</a>"),
                    "java-absent": AJS.I18n.getText("attach.screenshot.java.is.absent.unified", "<a target=\"_blank\" href=\"//java.com\">", "</a>"),
                    "java-security": AJS.I18n.getText("attach.screenshot.java.is.blocked.by.security.unified", "<a target=\"_blank\" href=\"//java.com\">", "</a>")
                };

                return fail(reason, messages[reason]);
            });
    });

    exports.getClipboardData = function(){
        var deferred = new $.Deferred();
        exports.install(document).done(function(){
            try {
                appletNode.reload();
                deferred.resolve(appletNode.getClipboardData());
            } catch(e){
                deferred.reject(AJS.I18n.getText("attach.screenshot.java.is.broken.unified"));
            }
        }).fail(function(reason, message){
            deferred.reject(message);
        });
        return deferred;
    };

    exports.isRequired = function () {
        // Enable the Java applet for IE 8/9/10, assume all other browsers are compatible, since we
        // support latest IE, Chrome, Firefox, and all of these support HTML5 clipboard.
        return version.isIE8() || version.isIE9() || version.isIE10();
    };

    exports.isRequiredForBinaryAjax = function () {
        // The applet has security restrictions on Windows 8 + IE10, so we can't use it there. Luckily IE10 supports
        // AJAX requests with binary data anyway, so we can use that. IE8 and IE9 don't support binary data.
        return version.isIE8() || version.isIE9();
    };

    // proxy ajax request, because session with temporary attachments is in applet
    exports.proxyAjaxRequest = function (fn, userTokenFn) {
        return function () {
            var _smartAjaxMakeRequest = SmartAjax.makeRequest;

            SmartAjax.makeRequest = function (requestOptions) {
                var requestParams = _.reduce(_.keys(requestOptions.data), function (r, name) {
                    r.push(name, requestOptions.data[name]);
                    return r;
                }, []);

                requestParams.push("secureToken", userTokenFn());

                var requestResult = appletNode.doAjaxRequest(requestOptions.url, "UTF-8", window.navigator.userAgent, $.Deferred(), requestParams);
                requestResult.abort = $.noop;
                requestResult.then(function (data) {
                    var responseHeaders = _.filter(arguments, function (el, idx) {
                        return idx > 0;
                    });
                    responseHeaders[0] = "Status";
                    responseHeaders = _.reduce(_.reduce(responseHeaders, function (r, el, idx) {
                        var pdx = (idx / 2) << 0;
                        (r[pdx] || (r[pdx] = [])).push(el);
                        return r;
                    }, []), function (r, el) {
                        r[el[0]] = el[1];
                        return r;
                    }, {});

                    var xhr = {
                        getResponseHeader: function (name) {
                            return responseHeaders[name];
                        }
                    };
                    var textStatus = 'success';
                    var smartAjaxResult = new SmartAjax.SmartAjaxResult(xhr, 0, "success", data, true);
                    requestOptions.complete(xhr, textStatus, smartAjaxResult);
                });

                return requestResult;
            };

            fn.apply(this, arguments);

            SmartAjax.makeRequest = _smartAjaxMakeRequest;
        };
    };
});