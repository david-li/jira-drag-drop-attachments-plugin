AJS.test.require(["com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources"], function () {
  "use strict";

  var polyfill = require("dndattachment/ctrlv/polyfill");
  var _ = require("underscore");
  var $ = require("jquery");

  module("dndattachment/ctrlv/polyfill", {
    setup: function () {
      this.sandbox = sinon.sandbox.create();
      this.context = AJS.test.mockableModuleContext();
    },
    teardown: function () {
      this.sandbox.restore();
    }
  });

  test("install API is a function", function (assert) {
    assert.equal(typeof polyfill.install, "function");
  });

  test("isRequired API returns a boolean", function (assert) {
    assert.ok([true, false].indexOf(polyfill.isRequired()) != -1);
  });

  test("isRequiredForBinaryAjax API returns a boolean", function (assert) {
    assert.ok([true, false].indexOf(polyfill.isRequiredForBinaryAjax()) != -1);
  });

  test("proxyAjaxRequest API is a function", function (assert) {
    assert.equal(typeof polyfill.proxyAjaxRequest, "function");
  });

  _.each({
      "1.7.0_40": "clipboard-legacy.jar",
      "1.7.0_45": "clipboard.jar"
  }, function (archive, version) {
    test(".install() uses " + version + " for Java " + archive, function (assert) {

      var appletNode = this.sandbox.stub();
      var contentWindow = this.sandbox.stub();
      var contextPath = "/prefix/";
      var deployJava = this.sandbox.stub();

      // Stub out the resource URI resolution.
      var resourceUrisMock = {get: this.sandbox.stub()};
      resourceUrisMock.get.withArgs("clipboard.jar").returns(contextPath + "clipboard.jar");
      resourceUrisMock.get.withArgs("clipboard-legacy.jar").returns(contextPath + "clipboard-legacy.jar");

      this.context.mock("dndattachment/ctrlv/resource-uris", resourceUrisMock);

      // Mock up the applet node. It needs to look like the clipboard Java applet that we use.
      appletNode.isSecurityOk = this.sandbox.stub().returns(true);

      // Mock up the content window. It needs to look like an iframe DOM node.
      contentWindow.document = {};
      contentWindow.document.getElementById = this.sandbox.stub().returns(appletNode);

      // Mock up deployJava.
      deployJava.runApplet = this.sandbox.stub();
      deployJava.getJREs = this.sandbox.stub().returns([version]);
      deployJava.versionCheck = this.sandbox.stub();
      deployJava.versionCheck.withArgs("1.7.0_06+").returns(true);
      deployJava.versionCheck.withArgs("1.7.0_45+").returns(version === "1.7.0_45");

      var deployJavaMock = {requireDeployJava: this.sandbox.stub()};
      deployJavaMock.requireDeployJava.returns(
        $.Deferred().resolve(deployJava, contentWindow).promise()
      );

      this.context.mock("dndattachment/ctrlv/requireDeployJava", deployJavaMock);

      var polyfillMocked = this.context.require("dndattachment/ctrlv/polyfill");

      polyfillMocked.install(document)
        .always(function () {
          assert.ok(resourceUrisMock.get.called);
          assert.equal(deployJava.runApplet.firstCall.args[0].archive, archive);
          assert.equal(deployJava.runApplet.firstCall.args[0].codebase, contextPath);
        });
    });
  });
});
