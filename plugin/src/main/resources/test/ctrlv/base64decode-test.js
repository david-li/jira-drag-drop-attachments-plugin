AJS.test.require(["com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources"], function() {
    "use strict";

    module("dndattachment/ctrlv/base64decode", {
        uint8arrayEqual: function (a, b) {
            equal(JSON.stringify(a), JSON.stringify(b));
        }
    });

    test("Decoding a valid UTF-8 encoded string should return the string", function () {
        var encoded = "YWJjZA==",
          expected = new Uint8Array([97, 98, 99, 100]);

        var encodeModule = require("dndattachment/ctrlv/base64decode");
        this.uint8arrayEqual(encodeModule.base64decode(encoded), expected);
    });
});
