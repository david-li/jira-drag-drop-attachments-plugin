AJS.test.require([
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources",
    "com.atlassian.plugins.atlassian-plugins-webresource-plugin:data"
], function () {
    "use strict";

    var $ = require("jquery");

    module("dndattachment/ctrlv/deployJava", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("object element should be rendered somewhere outside the viewport", function() {
        this.sandbox.stub(deployJava, "getBrowser").returns("MSIE");
        this.sandbox.stub(document, "write"); // we don't want the real document.write to be executed

        deployJava.writePluginTag();
        var $object = $(document.write.getCall(0).args[0]);

        equal($object.css("position"), "absolute", "object element must have position absolute");
        notEqual($object.css("top"), "auto", "object element must not have auto as top property");
    });

});







