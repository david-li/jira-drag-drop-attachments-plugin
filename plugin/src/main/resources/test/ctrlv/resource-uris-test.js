AJS.test.require([
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources",
    "com.atlassian.plugins.atlassian-plugins-webresource-plugin:data"
], function(){
    "use strict";

    module("dndattachment/ctrlv/resourceUris", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.dataKey = "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources.resource-uris";

            this.context = AJS.test.mockableModuleContext();
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    test("WRM.data is used to retrieve URIs", function (assert) {
        var data = {"key": "value"};
        this.sandbox.stub(WRM.data, "claim").withArgs(this.dataKey).returns(data);
        var resourceUris = this.context.require("dndattachment/ctrlv/resource-uris");
        equal(resourceUris.get("key"), "value");
        assert.ok(WRM.data.claim.called);
    });

    test(".get returns undefined for unknown resources", function (assert) {
        this.sandbox.stub(WRM.data, "claim").withArgs(this.dataKey).returns({});
        var resourceUris = this.context.require("dndattachment/ctrlv/resource-uris");
        assert.strictEqual(resourceUris.get("unknown"), undefined);
    });

    test(".get returns undefined when WRM.data doesn't provide any data", function (assert) {
        var resourceUris = this.context.require("dndattachment/ctrlv/resource-uris");
        assert.strictEqual(resourceUris.get("unknown"), undefined);
    });
});
