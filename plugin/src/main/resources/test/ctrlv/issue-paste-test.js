AJS.test.require([
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources",
    "com.atlassian.plugins.atlassian-plugins-webresource-plugin:data"
], function() {
    "use strict";

    module("dndattachment/ctrlv/issuePaste", {
        setup: function () {
            this.sandbox = sinon.sandbox.create();
            this.context = AJS.test.mockableModuleContext();

            /* event stub */
            this.keyPasteEvent = { target: {} };
            this.pasteEvent = {
                target: {},
                preventDefault: function () {}
            };
        },
        teardown: function () {
            this.sandbox.restore();
        }
    });

    var $ = require("jquery");
    var _ = require("underscore");
    var issue_paste = require("dndattachment/ctrlv/issue-paste");
    var utility = require("dndattachment/ctrlv/utility");
    var html5 = require("dndattachment/ctrlv/html5");

    function testTransition(currentState, expectedState, eventType, eventObject) {
        var newState = currentState(eventType, eventObject);
        equal(newState.stateName, expectedState.stateName);
        return newState;
    }

    /* prerequisites */
    test("states in stateMap should be unique", function () {
        var states = issue_paste._getStateMap().states;

        equal(_.keys(states).length, _.unique(_.values(states).map(function (state) {
            return state.stateName;
        })).length, "keys and unique values should have same length");
    });
    test("events in stateMap should be unique", function () {
        var events = issue_paste._getStateMap().events;
        equal(_.keys(events).length, _.unique(_.values(events)).length, "keys and unique values should have same length");
    });

    /* check transitions */
    test("idle should transition to catch clipboard after keydown", function () {
        this.stub(utility, "isKeyPasteEvent").withArgs(this.keyPasteEvent).returns(true);
        this.stub(utility, "browserIsNativePaste").returns(false);

        this.context.mock("dndattachment/ctrlv/utility", utility);

        var issue_paste_mocked = this.context.require("dndattachment/ctrlv/issue-paste");
        var states = issue_paste_mocked._getStateMap().states;
        var events = issue_paste_mocked._getStateMap().events;

        testTransition(new states.StateIdle(), states.StateCatchClipboard, events.EVENT_WINDOW_KEYDOWN, this.keyPasteEvent);
    });
    test("idle should transition to file loading after paste", function () {
        this.stub(utility, "isImagePasteEvent").withArgs(this.pasteEvent).returns(true);
        this.stub(html5, "getFileFromEvent").withArgs(this.pasteEvent).returns(new $.Deferred().promise());

        this.context.mock("dndattachment/ctrlv/utility", utility);
        this.context.mock("dndattachment/ctrlv/html5", html5);

        var issue_paste_mocked = this.context.require("dndattachment/ctrlv/issue-paste");
        var states = issue_paste_mocked._getStateMap().states;
        var events = issue_paste_mocked._getStateMap().events;

        testTransition(new states.StateIdle(), states.StateFileLoading, events.EVENT_WINDOW_PASTE, this.pasteEvent);
    });

    test("catch clipboard should transition to file loading after paste", function () {
        this.stub(utility, "isImagePasteEvent").withArgs(this.pasteEvent).returns(true);
        this.stub(html5, "getFileFromEvent").withArgs(this.pasteEvent).returns(new $.Deferred().promise());

        this.context.mock("dndattachment/ctrlv/utility", utility);
        this.context.mock("dndattachment/ctrlv/html5", html5);

        var issue_paste_mocked = this.context.require("dndattachment/ctrlv/issue-paste");
        var states = issue_paste_mocked._getStateMap().states;
        var events = issue_paste_mocked._getStateMap().events;

        testTransition(new states.StateCatchClipboard(this.keyPasteEvent), states.StateFileLoading, events.EVENT_WINDOW_PASTE, this.pasteEvent);
    });
    test("catch clipboard should transition to state idle after text paste", function () {
        this.stub(utility, "isImagePasteEvent").withArgs(this.pasteEvent).returns(false);
        this.stub(utility, "isTextPasteEvent").withArgs(this.pasteEvent).returns(true);
        this.stub(utility, "getTextPasteContent").withArgs(this.pasteEvent).returns("text");

        var insertToInput = this.stub(utility, "insertToInput");
        var preventDefault = this.stub(this.pasteEvent, "preventDefault");

        this.context.mock("dndattachment/ctrlv/utility", utility);

        var issue_paste_mocked = this.context.require("dndattachment/ctrlv/issue-paste");
        var states = issue_paste_mocked._getStateMap().states;
        var events = issue_paste_mocked._getStateMap().events;

        testTransition(new states.StateCatchClipboard(this.keyPasteEvent), states.StateIdle, events.EVENT_WINDOW_PASTE, this.pasteEvent);

        ok(insertToInput.calledOnce);
        ok(preventDefault.calledOnce);
    });

    test("file loading should transition to attach image after file load", function () {
        var file = new Blob([""], { type: "text/html" });

        this.stub(html5, "getFileFromEvent").withArgs(this.pasteEvent).returns(new $.Deferred().promise());
        this.stub(utility, "getCurrentIssueId").returns(12345);

        this.context.mock("dndattachment/ctrlv/html5", html5);
        this.context.mock("dndattachment/ctrlv/utility", utility);

        var issue_paste_mocked = this.context.require("dndattachment/ctrlv/issue-paste");
        var states = issue_paste_mocked._getStateMap().states;
        var events = issue_paste_mocked._getStateMap().events;
        testTransition(new states.StateFileLoading(this.pasteEvent), states.StateAttachImage, events.EVENT_FILE_LOADED, file);
    });

    test("attach image should transition to idle", function () {
        var file = {};
        var dialog = {};

        this.stub(html5, "show").withArgs().returns(new $.Deferred().promise());
        this.stub(utility, "convertBlobToImage");

        this.context.mock("dndattachment/ctrlv/html5", html5);
        this.context.mock("dndattachment/ctrlv/utility", utility);

        var issue_paste_mocked = this.context.require("dndattachment/ctrlv/issue-paste");
        var states = issue_paste_mocked._getStateMap().states;
        var events = issue_paste_mocked._getStateMap().events;
        testTransition(new states.StateAttachImage(file, this.pasteEvent), states.StateIdle, events.EVENT_DIALOG_LOADED, dialog);
    });

    /* check textarea manipulation */
    test("dialog cancel should preserve original text", function () {
        var ORIGINAL_TEXT = "test content";
        var file = {};
        var dialog = {};

        this.stub(utility, "isImagePasteEvent").withArgs(this.pasteEvent).returns(true);
        this.stub(utility, "isKeyPasteEvent").withArgs(this.keyPasteEvent).returns(true);
        this.stub(utility, "getTextPasteContent").withArgs(this.pasteEvent).returns("text");
        this.stub(utility, "browserIsNativePaste").returns(false);
        this.stub(utility, "getCurrentIssueId").returns(12345);
        this.stub(utility, "convertBlobToImage");

        this.stub(html5.dialogView, "appendBlobImage").withArgs(file);
        this.stub(html5, "getFileFromEvent").withArgs(this.pasteEvent).returns(new $.Deferred().promise());
        this.stub(html5, "validateFileSize");

        var $fixture = $('#qunit-fixture');
        var textarea = $('<textarea class="wiki-textfield"></textarea>').text(ORIGINAL_TEXT).appendTo($fixture)[0];

        // set selection start, canceled operation should not erase it
        textarea.selectionStart = 2;
        textarea.selectionEnd = ORIGINAL_TEXT.length - 1;

        this.context.mock("dndattachment/ctrlv/html5", html5);
        this.context.mock("dndattachment/ctrlv/utility", utility);

        var issue_paste_mocked = this.context.require("dndattachment/ctrlv/issue-paste");
        var states = issue_paste_mocked._getStateMap().states;
        var events = issue_paste_mocked._getStateMap().events;


        var stateIdle = new states.StateIdle();
        var stateCatchClipboard = testTransition(stateIdle, states.StateCatchClipboard, events.EVENT_WINDOW_KEYDOWN, this.keyPasteEvent);
        var stateFileLoading = testTransition(stateCatchClipboard, states.StateFileLoading, events.EVENT_WINDOW_PASTE, this.pasteEvent);
        var stateAttachImage = testTransition(stateFileLoading, states.StateAttachImage, events.EVENT_FILE_LOADED, file);
        testTransition(stateAttachImage, states.StateIdle, events.EVENT_DIALOG_CANCELED);

        equal(ORIGINAL_TEXT, textarea.value);
    });
});
