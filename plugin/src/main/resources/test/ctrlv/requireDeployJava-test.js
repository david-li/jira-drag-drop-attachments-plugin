AJS.test.require(["com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources"], function () {
    "use strict";

    module("dndattachment/ctrlv/requireDeployJava", {
        setup: function () {
            this.context = AJS.test.mockableModuleContext();
        }
    });

    test("uses dndattachment/ctrlv/resourceUris to find deployJava.html", sinon.test(function (assert) {
        var resourceUrisMock = { get: this.sandbox.stub() };
        resourceUrisMock.get.withArgs("deployJava.html").returns("/prefix/deployJava.html");

        this.context.mock("dndattachment/ctrlv/resource-uris", resourceUrisMock);

        var module = this.context.require("dndattachment/ctrlv/requireDeployJava");
        module.requireDeployJava();
        assert.ok(resourceUrisMock.get.calledOnce);
        assert.deepEqual(resourceUrisMock.get.firstCall.args, ["deployJava.html"]);
    }));

    test("returns the same object on every call", function (assert) {
        var module = require("dndattachment/ctrlv/requireDeployJava");
        var first = module.requireDeployJava(),
            second = module.requireDeployJava();

        assert.strictEqual(first, second);
    });
});
