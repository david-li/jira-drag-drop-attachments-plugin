AJS.test.require([
    "jira.webresources:require-shim",
    "jira.webresources:jira-global",
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:drag-and-drop-attachment-javascript"
], function(){
    "use strict";

    function test(name, callback) {
        asyncTest(name, function() {
            require(['dndattachment/dropzones/AttachmentsDropZone', 'jquery'], function(AttachmentsDropZone, $) {
                start();
                callback.apply(this, arguments)
            }.bind(this));
        });
    }

    module("AttachmentsDropZone", {
        setup: function() {
            this.sandbox = sinon.sandbox.create();
            JIRA.Events.LOCK_PANEL_REFRESHING = "lockPanelRefreshing";
            JIRA.Events.REFRESH_ISSUE_PAGE = "refreshIssuePage";
            JIRA.Issues = {Api:{
                getSelectedIssueId: function() {
                    return 12345;
                }
            }};
        },

        teardown: function() {
            this.sandbox.restore();
            delete JIRA.Events.LOCK_PANEL_REFRESHING;
            delete JIRA.Events.REFRESH_ISSUE_PAGE;
            delete JIRA.Issues;
        }
    });

    test("AttachmentsDropZone commitUpload test", function(AttachmentsDropZone, $) {
        var $fixture = $('#qunit-fixture');
        var requestDeferred = new $.Deferred();
        this.sandbox.stub(JIRA.SmartAjax, "makeRequest").returns(requestDeferred);
        this.sandbox.stub(JIRA, "trigger");

        var instance = new (Class.extend({
            $node: $fixture,
            queueTask: this.sandbox.stub(),
            queueEvent: this.sandbox.stub(),
            attachFile: AttachmentsDropZone.prototype.attachFile,
            getViewMode: function() {
                return "gallery"
            }
        }));

        var files = [1,2,3];
        var attachments = [];

        var commit = AttachmentsDropZone.prototype.commitUpload.call(instance, files);
        ok(commit != null, "commit is not null");
        ok(JIRA.SmartAjax.makeRequest.calledOnce, "makeRequest called once");

        var args = JIRA.SmartAjax.makeRequest.getCall(0).args[0];
        ok(args.data.filetoconvert.join() == files.join(), 'fileIDs in request');

        stop();
        requestDeferred.resolve(attachments);
        commit.then(function() {
            start();
            ok(JIRA.trigger.calledOnce, "trigger called once");
        });
    });

    test("AttachmentsDropZone commitUpload markDirty test", function(AttachmentsDropZone, $) {
        var $fixture = $('#qunit-fixture');
        var instance = new (AttachmentsDropZone.extend({
            $node: $fixture,
            pendingQueue: [],
            markDirty: this.sandbox.stub(),
            queueTask: AttachmentsDropZone.prototype.queueTask
        }))();

        var requestDeferred = new $.Deferred();
        this.sandbox.stub(JIRA.SmartAjax, "makeRequest").returns(requestDeferred);
        this.sandbox.stub(JIRA, "trigger");

        AttachmentsDropZone.prototype.commitUpload.call(instance, []);
        ok(instance.markDirty.calledWith(true), 'markDirty called with true');

        var args = JIRA.SmartAjax.makeRequest.getCall(0).args[0];

        var attachments = [];

        requestDeferred.resolve(attachments);
        ok(instance.markDirty.calledWith(false), 'markDirty called with false');
    });

    test("AttachmentsDropZone insert progress bar should respect sorting", function(AttachmentsDropZone, $) {
        var $fixture = $('#qunit-fixture');
        var $node = $('<div><div></div><ul id="file_attachments" data-sort-key="fileName" data-sort-order="asc"></ul></div>').appendTo($fixture);
        var $listNode = $node.find('#file_attachments');

        var $attachment1 = $('<li><span class="attachment-title">AAA</span></li>').appendTo($listNode);
        var $attachment2 = $('<li><span class="attachment-title">CCC</span></li>').appendTo($listNode);

        var $progressBar = $('<li><span class="attachment-title">BBB</span></li>');

        var attachmentsDropZone = new AttachmentsDropZone($node.find('div')[0]);
        attachmentsDropZone.render();
        attachmentsDropZone.insertProgressBar($progressBar);

        ok($progressBar.prev().is($attachment1), "attachment1 is previous one");
        ok($progressBar.next().is($attachment2), "attachment2 is next one");
    });
});
