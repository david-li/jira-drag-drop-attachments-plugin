AJS.test.require([
    "jira.webresources:require-shim",
    "jira.webresources:jira-global",
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:drag-and-drop-attachment-javascript"
], function(){
    "use strict";

    function test(name, callback) {
        asyncTest(name, function (assert) {
            require([
                'dndattachment/upload/handler',
                'dndattachment/upload/default/executor',
                'underscore',
                'jquery'
            ], function (UploadHandler, DefaultExecutor, _, $) {
                start();
                callback.apply(this, _.flatten([arguments, assert]));
            }.bind(this));
        });
    }

    module("UploadHandler", {
        setup: function () {
        },

        teardown: function () {
        }
    });

    var setupTest = function(UploadHandler, DefaultExecutor) {
        // Init the handler so it's listening
        UploadHandler.initialize();
        UploadHandler.registerExecutor(DefaultExecutor);
    };

    var teardownTest = function(UploadHandler, DefaultExecutor) {
        // Disable the handler
        UploadHandler.unregisterExecutor(DefaultExecutor);
        UploadHandler.disable();
    };

    test("registerExecutor validation test", function (UploadHandler, DefaultExecutor, _, $, assert) {
        var validExecutor = {
            name: "Dummy",
            weight: 10, // higher than the default
            isValid: function() {
                return false;
            },
            processFiles: function() {
                return null;
            }
        };
        assert.equal(UploadHandler.registerExecutor(validExecutor), true);

        var nonameExecutor = {
            weight: 10, // higher than the default
            isValid: function() {
                return false;
            },
            processFiles: function() {
                return null;
            }
        };
        assert.equal(UploadHandler.registerExecutor(nonameExecutor), false);

        var noweightExecutor = {
            name: "Dummy",
            isValid: function() {
                return false;
            },
            processFiles: function() {
                return null;
            }
        };
        assert.equal(UploadHandler.registerExecutor(noweightExecutor), false);

        var noisValidExecutor = {
            name: "Dummy",
            weight: 10, // higher than the default
            processFiles: function() {
                return null;
            }
        };
        assert.equal(UploadHandler.registerExecutor(noisValidExecutor), false);

        var noProcessFilesExecutor = {
            name: "Dummy",
            weight: 10, // higher than the default
            isValid: function() {
                return false;
            }
        };
        assert.equal(UploadHandler.registerExecutor(noProcessFilesExecutor), false);

        var emptyExecutor = {};
        assert.equal(UploadHandler.registerExecutor(emptyExecutor), false);
    });

    test("registerExecutor valid executor is not called when isValid returns false", function (UploadHandler, DefaultExecutor, _, $, assert) {
        setupTest(UploadHandler, DefaultExecutor);

        //Set up the test
        var Events = require('jira/util/events');
        var EventTypes = require('dndattachment/util/events/types');
        var dummyProcessFiles = sinon.stub();
        var defaultProcessFiles = sinon.stub();
        var result = new $.Deferred();

        var dummyExecutor = {
            name: "Dummy",
            weight: 10, // higher than the default
            isValid: function() {
                return false;
            },
            processFiles: function() {
                dummyProcessFiles();
                return result;
            }
        };

        var fakeAttachmentDropZone = {
            uploadFiles: function () {
                defaultProcessFiles();
                return result;
            }
        };
        UploadHandler.setAttachmentDropZone(fakeAttachmentDropZone);
        // Attach the dummy executor
        UploadHandler.registerExecutor(dummyExecutor);

        // Throw an event and hope it fires
        Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
            files: [1, 2, 3]
        });

        result.resolve().always(function () {
            sinon.assert.notCalled(dummyProcessFiles, "the dummy was not called");
            sinon.assert.calledOnce(defaultProcessFiles, "the default was called");

            UploadHandler.unregisterExecutor(dummyExecutor);
            teardownTest(UploadHandler, DefaultExecutor);
        });
    });

    test("registerExecutor valid executor is called when isvalid returns true", function (UploadHandler, DefaultExecutor, _, $, assert) {
        setupTest(UploadHandler, DefaultExecutor);

        //Set up the test
        var Events = require('jira/util/events');
        var EventTypes = require('dndattachment/util/events/types');
        var dummyProcessFiles = sinon.stub();
        var defaultProcessFiles = sinon.stub();
        var result = new $.Deferred();

        var dummyExecutor = {
            name: "Dummy",
            weight: 10, // higher than the default
            isValid: function() {
                return true;
            },
            processFiles: function() {
                dummyProcessFiles();
                return result;
            }
        };

        var fakeAttachmentDropZone = {
            uploadFiles: function () {
                defaultProcessFiles();
                return result;
            }
        };
        UploadHandler.setAttachmentDropZone(fakeAttachmentDropZone);
        // Attach the dummy executor
        UploadHandler.registerExecutor(dummyExecutor);

        // Throw an event and hope it fires
        Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
            files: [1, 2, 3]
        });

        result.resolve().always(function () {
            sinon.assert.calledOnce(dummyProcessFiles, "The dummy was called once");
            sinon.assert.notCalled(defaultProcessFiles, "the default was not called");

            UploadHandler.unregisterExecutor(dummyExecutor);
            teardownTest(UploadHandler, DefaultExecutor);
        });
    });

    test("handleAttachmentReceived success test", function (UploadHandler, DefaultExecutor, _, $, assert) {
        setupTest(UploadHandler, DefaultExecutor);

        //Set up the test
        var Events = require('jira/util/events');
        var EventTypes = require('dndattachment/util/events/types');

        var success = sinon.stub();
        var failure = sinon.stub();

        var result = new $.Deferred();

        var fakeAttachmentDropZone = {
            uploadFiles: function () {
                return result;
            }
        };

        UploadHandler.setAttachmentDropZone(fakeAttachmentDropZone);
        // Throw an event and hope it fires
        Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
            files: [1, 2, 3],
            successCallback: success,
            failureCallback: failure
        });

        result.resolve().always(function () {
            sinon.assert.calledOnce(success, "success method was called once");
            sinon.assert.notCalled(failure, "failure method was not called");

            teardownTest(UploadHandler, DefaultExecutor);
        });
    });

    test("handleAttachmentReceived failure test", function (UploadHandler, DefaultExecutor, _, $, assert) {
        setupTest(UploadHandler, DefaultExecutor);

        //Set up the test
        var Events = require('jira/util/events');
        var EventTypes = require('dndattachment/util/events/types');

        var success = sinon.stub();
        var failure = sinon.stub();

        var result = new $.Deferred();

        var fakeAttachmentDropZone = {
            uploadFiles: function () {
                return result;
            }
        };

        UploadHandler.setAttachmentDropZone(fakeAttachmentDropZone);
        // Throw an event and hope it fires
        Events.trigger(EventTypes.ATTACHMENT_FOR_PAGE_RECEIVED, {
            files: [1, 2, 3],
            successCallback: success,
            failureCallback: failure
        });

        result.reject().always(function () {
            sinon.assert.notCalled(success, "success method was not once");
            sinon.assert.calledOnce(failure, "failure method was called once");

            teardownTest(UploadHandler, DefaultExecutor);
        });
    });
});