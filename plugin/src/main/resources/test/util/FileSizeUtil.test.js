AJS.test.require(["com.atlassian.jira.plugins.jira-dnd-attachment-plugin:drag-and-drop-attachment-javascript"], function(){
    "use strict";

    function test(name, callback) {
        asyncTest(name, function (assert) {
            require(['dndattachment/util/FileSizeUtil'], function (FileSizeUtil) {
                start();
                callback.call(this, assert, FileSizeUtil);
            }.bind(this));
        });
    }

    module("FileSizeUtil");

    test("FileSizeUtil test", function (assert, FileSizeUtil) {
        assert.equal(FileSizeUtil.format(512), "0.5 kB");
        assert.equal(FileSizeUtil.format(1024), "1.0 kB");
        assert.equal(FileSizeUtil.format(2048), "2 kB");
        assert.equal(FileSizeUtil.format(1024 * 400), "400 kB");
        assert.equal(FileSizeUtil.format(1024 * 1024), "1024 kB");
        assert.equal(FileSizeUtil.format(1024 * 1024 * 1.2), "1.20 MB");
        assert.equal(FileSizeUtil.format(1024 * 1024 * 20), "20.00 MB");
    });

});