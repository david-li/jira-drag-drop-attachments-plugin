AJS.test.require([
    "jira.webresources:require-shim",
    "jira.webresources:jira-global",
    "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:drag-and-drop-attachment-javascript"
], function(){
    "use strict";

    function test(name, deps, callback) {
        if(arguments.length == 2)
            callback = deps, deps = ['dndattachment/Parser', 'jquery'];
        return asyncTest(name, function() {
            require(deps, function() {
                start();
                callback.apply(this, arguments);
            }.bind(this));
        })
    }

    module("TestParser", {
        setup: function() {
            this.sandbox = sinon.sandbox.create();
        },

        teardown: function() {
            this.sandbox.restore();
        }
    });

    test("Parser test", function(Parser, $) {
        var $fixture = $('#qunit-fixture');

        ok(Parser.parse, 'Parser.parse is not null');
        ok(typeof Parser.parse == "function", 'Parser.parse is function');

        $('<span duiType="TestDuiType"> </span>').appendTo($fixture);

        var TestDuiTypeConstructor = this.sandbox.stub();
        define('TestDuiType', function() {
            return TestDuiTypeConstructor;
        });

        stop();
        $.when.apply(window, [Parser.parse($fixture), Parser.parse($fixture)]).then(function() {
            start();
            ok(TestDuiTypeConstructor.calledOnce, 'TestDuiTypeConstructor called once');
        });
    });
});

