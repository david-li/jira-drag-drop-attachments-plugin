package com.atlassian.jira.plugins.dnd.attachment.providers;

import com.atlassian.jira.bc.JiraServiceContext;
import com.atlassian.jira.bc.JiraServiceContextImpl;
import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.plugin.webfragment.contextproviders.AbstractJiraContextProvider;
import com.atlassian.jira.plugin.webfragment.model.JiraHelper;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.SimpleErrorCollection;
import webwork.config.Configuration;

import java.util.HashMap;
import java.util.Map;

/*
 * This class provides the dnd-metadata webpanel with some metadata that
 * is consumed by this plugin and by other plugins.
 */
public class MetadataContextProvider extends AbstractJiraContextProvider
{

    private final AttachmentService attachmentService;

    public MetadataContextProvider(AttachmentService attachmentService)
    {
        this.attachmentService = attachmentService;
    }

    @Override
    public Map getContextMap(ApplicationUser applicationUser, JiraHelper jiraHelper)
    {
        Map<String, String> map = new HashMap<String, String>();

        Project project = jiraHelper.getProject();

        // if project is null, canAttach returns false
        map.put("canAttach", canAttach(applicationUser, project).toString());

        if (project != null && project.getProjectTypeKey() != null)
        {
            map.put("projectType", project.getProjectTypeKey().getKey());
        }
        else
        {
            map.put("projectType", "");
        }

        map.put("uploadLimit", Configuration.getString(APKeys.JIRA_ATTACHMENT_SIZE));

        return map;
    }

    private Boolean canAttach(ApplicationUser applicationUser, Project project)
    {
        JiraServiceContext context = new JiraServiceContextImpl(applicationUser, new SimpleErrorCollection());
        return attachmentService.canCreateAttachments(context, project);
    }
}
