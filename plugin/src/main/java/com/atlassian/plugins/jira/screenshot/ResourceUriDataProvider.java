package com.atlassian.plugins.jira.screenshot;

import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.json.marshal.Jsonable;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.atlassian.webresource.api.data.WebResourceDataProvider;

import java.io.IOException;
import java.io.Writer;

public class ResourceUriDataProvider implements WebResourceDataProvider {
    private static final String WEB_RESOURCE = "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources";

    private final WebResourceUrlProvider webResourceUrlProvider;

    public ResourceUriDataProvider(WebResourceUrlProvider webResourceUrlProvider) {
        this.webResourceUrlProvider = webResourceUrlProvider;
    }

    @Override
    public Jsonable get()
    {
        return new Jsonable() {
            @Override
            public void write(Writer writer) throws IOException {
                JSONObject map = new JSONObject();

                try
                {
                    map.put("deployJava.html", uri("deployJava.html"));
                    map.put("clipboard.jar", uri("clipboard.jar"));
                    map.put("clipboard-legacy.jar", uri("clipboard-legacy.jar"));
                    map.write(writer);
                }
                catch (JSONException e)
                {
                    throw new RuntimeException(e);
                }
            }
        };
    }

    private String uri(String name)
    {
        return webResourceUrlProvider.getStaticPluginResourceUrl(WEB_RESOURCE, name, UrlMode.AUTO);
    }
}
