package com.atlassian.plugins.jira.screenshot.webwork;

import com.atlassian.core.util.map.EasyMap;
import com.atlassian.jira.bc.issue.attachment.AttachmentService;
import com.atlassian.jira.bc.issue.comment.CommentService;
import com.atlassian.jira.config.SubTaskManager;
import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.event.issue.IssueEventSource;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.exception.IssueNotFoundException;
import com.atlassian.jira.exception.IssuePermissionException;
import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.issue.TemporaryAttachmentsMonitorLocator;
import com.atlassian.jira.issue.attachment.Attachment;
import com.atlassian.jira.issue.attachment.TemporaryAttachment;
import com.atlassian.jira.issue.fields.FieldManager;
import com.atlassian.jira.issue.fields.screen.FieldScreenRendererFactory;
import com.atlassian.jira.issue.history.ChangeItemBean;
import com.atlassian.jira.issue.util.IssueUpdateBean;
import com.atlassian.jira.issue.util.IssueUpdater;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.roles.ProjectRoleManager;
import com.atlassian.jira.security.xsrf.RequiresXsrfCheck;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.SecureUserTokenManager;
import com.atlassian.jira.user.util.UserUtil;
import com.atlassian.jira.util.Function;
import com.atlassian.jira.util.collect.CollectionUtil;
import com.atlassian.jira.web.action.issue.AbstractCommentableIssue;
import com.atlassian.jira.web.action.issue.TemporaryAttachmentsMonitor;
import com.atlassian.jira.web.util.AttachmentException;
import com.atlassian.security.random.DefaultSecureTokenGenerator;
import com.atlassian.security.random.SecureTokenGenerator;
import org.apache.commons.lang.StringUtils;
import org.ofbiz.core.entity.GenericEntityException;
import org.ofbiz.core.util.UtilDateTime;
import webwork.action.ActionContext;
import webwork.config.Configuration;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.atlassian.jira.util.dbc.Assertions.notNull;

/**
 * Handles all actions in JIRA attach image dialog
 */
public class ShowAttachScreenshotFormAction extends AbstractCommentableIssue
{

    private static final String SCREENSHOT_PREFIX = "screenshot-";
    private static final Pattern SHOT_NUMBER_REGEX = Pattern.compile("screenshot-(\\d+)");

    private static final String HEADER_USER_AGENT = "user-agent";
    private static final String MAC_OS_IDENTIFIER = "Mac";

    private static final String PLATFORM_MAC = "mac";
    private static final String PLATFORM_PC = "pc";

    private long maxSize = Long.MIN_VALUE;

    private final AttachmentService attachmentService;
    private final TemporaryAttachmentsMonitorLocator temporaryAttachmentsMonitorLocator;
    private final IssueUpdater issueUpdater;
    private final SecureUserTokenManager secureUserTokenManager;
    private final JiraAuthenticationContext jiraAuthenticationContext;
    private final SecureTokenGenerator secureTokenGenerator = DefaultSecureTokenGenerator.getInstance();

    // Copied from AttachFile (action) JIRA Core
    private String[] filetoconvert;
    private static final String FILETOCONVERT = "filetoconvert";

    private String attachScreenshotFilename;

    private String secureToken;

    private String formToken;

    public ShowAttachScreenshotFormAction(final SubTaskManager subTaskManager,
            final FieldScreenRendererFactory fieldScreenRendererFactory,
            final FieldManager fieldManager, final ProjectRoleManager projectRoleManager,
            final CommentService commentService, final UserUtil userUtil,
            final AttachmentService attachmentService, final TemporaryAttachmentsMonitorLocator temporaryAttachmentsMonitorLocator,
            final IssueUpdater issueUpdater, final SecureUserTokenManager secureUserTokenManager,
            final JiraAuthenticationContext jiraAuthenticationContext)
    {
        super(subTaskManager, fieldScreenRendererFactory, fieldManager, projectRoleManager, commentService, userUtil);
        this.attachmentService = attachmentService;
        this.temporaryAttachmentsMonitorLocator = temporaryAttachmentsMonitorLocator;
        this.issueUpdater = issueUpdater;
        this.secureUserTokenManager = secureUserTokenManager;
        this.jiraAuthenticationContext = jiraAuthenticationContext;
    }

    @Override
    public String doDefault() throws Exception
    {
        if (getFormToken() == null)
        {
            setFormToken(secureTokenGenerator.generateToken());
        }

        try
        {
            attachmentService.canCreateAttachments(getJiraServiceContext(), getIssueObject());
        }
        catch (final IssueNotFoundException e)
        {
            // Error is added above
            return ERROR;
        }
        catch (final IssuePermissionException e)
        {
            return ERROR;
        }
        return INPUT;
    }

    /**
     * Returns the latest file name followed by naming pattern 'screenshot-' starting from First ever name suggested is
     * 'screenshot-1'
     *
     * @return suggested file name
     */
    public String getNextScreenshotName()
    {
        return SCREENSHOT_PREFIX + getNextScreenshotNumber();
    }

    /**
     * This is a workaround for IE8-9 FormData upload problem, we can't use IFrame workaround and in
     * Java Applet/Flash player we can not have JSESSIONID cookie, because it is httpOnly. For those reasons
     * we need to use this token.
     *
     * @return String
     */

    public String getNewUserToken()
    {
        return secureUserTokenManager.generateToken(getLoggedInUser(), SecureUserTokenManager.TokenType.SCREENSHOT);
    }

    /**
     * Checks if user uses Mac or PC
     *
     * @return string "mac" or "pc"
     */
    public String getUserPlatform()
    {
        HttpServletRequest request = ActionContext.getRequest();
        String userAgent = request.getHeader(HEADER_USER_AGENT);
        if (StringUtils.contains(userAgent, MAC_OS_IDENTIFIER))
        {
            return PLATFORM_MAC;
        }
        return PLATFORM_PC;
    }

    public int getNextScreenshotNumber()
    {
        int last = 0;
        for (final Attachment attachment : getIssueObject().getAttachments())
        {
            final Matcher matcher = SHOT_NUMBER_REGEX.matcher(attachment.getFilename());
            if (matcher.find())
            {
                try
                {
                    last = Math.max(last, Integer.parseInt(matcher.group(1)));
                }
                catch (RuntimeException impossible)
                {
                    log.info("problem parsing screenshot number in " + attachment.getFilename(), impossible);
                }
            }
        }
        return last + 1;
    }

    public long getMaxSize()
    {
        if (maxSize != Long.MIN_VALUE)
        {
            return maxSize;
        }

        try
        {
            maxSize = Long.parseLong(Configuration.getString(APKeys.JIRA_ATTACHMENT_SIZE));
        }
        catch (NumberFormatException e)
        {
            maxSize = -1;
        }
        return maxSize;
    }

    public String[] getFiletoconvert()
    {
        return filetoconvert;
    }

    public void setFiletoconvert(final String[] filetoconvert)
    {
        this.filetoconvert = filetoconvert;
    }

    public String getAttachscreenshotname()
    {
        return attachScreenshotFilename;
    }

    public void setAttachscreenshotname(final String attachScreenshotFilename)
    {
        this.attachScreenshotFilename = attachScreenshotFilename;
    }

    public String getSecureToken() {
        return secureToken;
    }

    public void setSecureToken(String secureToken) {
        this.secureToken = secureToken;
    }

    public String getFormToken()
    {
        return formToken;
    }

    public void setFormToken(final String formToken)
    {
        this.formToken = formToken;
    }

    @Override
    protected void doValidation()
    {
        try
        {
            if(secureToken != null)
            {
                jiraAuthenticationContext.setLoggedInUser(secureUserTokenManager.useToken(secureToken, SecureUserTokenManager.TokenType.SCREENSHOT));
            }

            attachmentService.canCreateAttachments(getJiraServiceContext(), getIssueObject());
            super.doValidation(); // validate comment
        }
        catch (final IssueNotFoundException ex)
        {
            // Do nothing as error is added above
            return;
        }
        catch (final IssuePermissionException ex)
        {
            // Do nothing as error is added above
            return;
        }
    }

    @Override
    public Map<String, Object> getDisplayParams()
    {
        final Map<String, Object> displayParams = new HashMap<String, Object>(super.getDisplayParams());
        displayParams.put("theme", "aui");
        return displayParams;
    }

    private void clearTemporaryAttachmentsForIssue() throws GenericEntityException
    {
        temporaryAttachmentsMonitorLocator.get(true).clearEntriesForIssue(getId());
    }

    private List<Long> getTemporaryFileIdsToConvert()
    {
        final String[] strings = getFiletoconvert();
        if (strings == null)
        {
            return Collections.emptyList();
        }
        final List<String> fileIdStrings = Arrays.asList(strings);
        return CollectionUtil.transform(fileIdStrings, new Function<String, Long>() {
            public Long get(final String input) {
                return Long.parseLong(input);
            }
        });
    }

    @RequiresXsrfCheck
    @Override
    protected String doExecute() throws Exception
    {
        final Issue issue = getIssueObject();

        final Collection<ChangeItemBean> changeItemBeans = new ArrayList<ChangeItemBean>();
        final List<Long> fileIdsToConvert = getTemporaryFileIdsToConvert();
        try
        {
            final TemporaryAttachmentsMonitor temporaryAttachmentsMonitor = temporaryAttachmentsMonitorLocator.get(false);
            if (temporaryAttachmentsMonitor != null)
            {
                String fileNameWithExt = getWithExtenstion(this.attachScreenshotFilename);
                changeItemBeans.addAll(this.convertTemporaryAttachments(getLoggedInUser(), getIssueObject(), fileIdsToConvert, temporaryAttachmentsMonitor, fileNameWithExt));
            }
            else
            {
                addError(FILETOCONVERT, getText("attachment.temporary.session.time.out"));
                return ERROR;
            }
        }
        catch (final AttachmentException e)
        {
            addError(FILETOCONVERT, e.getMessage());
            return ERROR;
        }

        clearTemporaryAttachmentsForIssue();

        final IssueUpdateBean issueUpdateBean = new IssueUpdateBean(getIssue(), getIssue(), EventType.ISSUE_UPDATED_ID, getLoggedInUser());
        issueUpdateBean.setComment(createComment());
        issueUpdateBean.setChangeItems(changeItemBeans);
        issueUpdateBean.setDispatchEvent(true);
        issueUpdateBean.setParams(EasyMap.build("eventsource", IssueEventSource.ACTION));

        issueUpdater.doUpdate(issueUpdateBean, true);

        if (isInlineDialogMode())
        {
            return returnComplete();
        }

        return getRedirect("/browse/" + issue.getKey());
    }

    /**
     * Adds a .png file extension if a file does not have one yet.
     *
     * NOTE: Currently clipboard works as PNG in Linux/Mac
     * @param fileName a file name
     * @return file name with a png extension
     */
    private String getWithExtenstion(String fileName)
    {
        if (StringUtils.endsWithIgnoreCase(fileName, ".png"))
        {
            return fileName;
        }
        return fileName + ".png";
    }


    private List<ChangeItemBean> convertTemporaryAttachments(final ApplicationUser user, final Issue issue, final List<Long> selectedAttachments, final TemporaryAttachmentsMonitor temporaryAttachmentsMonitor, final String attachScreenshotFileName)
            throws AttachmentException
    {
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        // NOTE: this method has been copied from attachment manager and changed to accept fileName specified in Screenshot Attachment Dialog
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        notNull("issue", issue);
        notNull("attachScreenshotFileName", attachScreenshotFileName);
        notNull("selectedAttachments", selectedAttachments);
        notNull("temporaryAttachmentsMonitor", temporaryAttachmentsMonitor);

        final List<ChangeItemBean> ret = new ArrayList<ChangeItemBean>();
        for (final Long selectedAttachment : selectedAttachments)
        {
            final TemporaryAttachment tempAttachment = temporaryAttachmentsMonitor.getById(selectedAttachment);
            final ChangeItemBean cib = attachmentManager.createAttachment(tempAttachment.getFile(), attachScreenshotFileName, tempAttachment.getContentType(), user, issue, Collections.<String, Object>emptyMap(), UtilDateTime.nowTimestamp());
            if (cib != null)
            {
                ret.add(cib);
            }
        }

        //finally clear any other remaining temp attachments for give form token
        temporaryAttachmentsMonitor.clearEntriesForFormToken(formToken);
        return ret;
    }
}
