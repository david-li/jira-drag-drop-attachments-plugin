package com.atlassian.plugins.jira.screenshot;

import com.atlassian.jira.util.json.JSONException;
import com.atlassian.jira.util.json.JSONObject;
import com.atlassian.plugin.webresource.UrlMode;
import com.atlassian.plugin.webresource.WebResourceUrlProvider;
import com.google.common.collect.Maps;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.StringWriter;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.when;

@RunWith (MockitoJUnitRunner.class)
public class TestResourceUriDataProvider {
    private static final String WEB_RESOURCE = "com.atlassian.jira.plugins.jira-dnd-attachment-plugin:jira-html5-attach-images-resources";

    @Mock
    private WebResourceUrlProvider urlProvider;

    @Test
    public void getShouldProvideUrisForJavaAppletResources() throws Exception
    {
        when(urlProvider.getStaticPluginResourceUrl(WEB_RESOURCE, "deployJava.html", UrlMode.AUTO)).thenReturn("prefixed/deployJava.html");
        when(urlProvider.getStaticPluginResourceUrl(WEB_RESOURCE, "clipboard.jar", UrlMode.AUTO)).thenReturn("prefixed/clipboard.jar");
        when(urlProvider.getStaticPluginResourceUrl(WEB_RESOURCE, "clipboard-legacy.jar", UrlMode.AUTO)).thenReturn("prefixed/clipboard-legacy.jar");
        ResourceUriDataProvider dataProvider = new ResourceUriDataProvider(urlProvider);
        Map<String, String> expected = Maps.newHashMap();
        expected.put("deployJava.html", "prefixed/deployJava.html");
        expected.put("clipboard.jar", "prefixed/clipboard.jar");
        expected.put("clipboard-legacy.jar", "prefixed/clipboard-legacy.jar");

        StringWriter writer = new StringWriter();
        dataProvider.get().write(writer);
        String json = writer.getBuffer().toString();

        JSONObject actual = new JSONObject(json);
        assertJsonObjectEqual(actual, expected);
    }

    private void assertJsonObjectEqual(JSONObject json, Map<String, String> expected)
    {
        assertEquals("both objects should have the same number of attributes", expected.size(), json.length());
        for (String key : expected.keySet())
        {
            try
            {
                assertEquals(expected.get(key), json.getString(key));
            }
            catch (JSONException e)
            {
                fail("JSON attribute '" + key + "' could not be retrieved.");
            }
        }
    }
}
